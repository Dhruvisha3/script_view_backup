
rem
rem Usage:
rem		call CreateLmwInstallImage <ImageName> <TargetDir> <VisioSourceDir> <VisioC2RSourceDir>
rem
rem <ImageName>   is informational only, it is only used in trace output.
rem <TargetDir>   is the fully qualified name of the LonMaker installation image (i.e., \\Virgil\...)
rem <VisioSource> is the fully qualified name of the directory containing the Visio image
rem <VisioC2RSource> is the fully qualified name of the directory containing the Visio ClickToRun image
rem					If no Visio, specify ""
rem Parameters must be in quotes if they contain spaces
rem

set ImageName=%~1
set TargetDir=%~2
set VisioDir=%~3
set VisioC2RDir=%~4
set AdobeInstaller=%LMWBldVerRoot%\build\bin\%adobeReader%
set LmInstallImage=%IsProjPath%\LonMaker\LonMaker\Release\DiskImages\DISK1
set VisioupdateDir=%git_component%Others\Microsoft\Visio\Visio2016\VisioUpdate

rem suppress xcopy output:
rem
set xcOpt=/Q

echo ..
echo %date% %time% Creating LonMaker %ImageName% Installation Image
echo ..
echo .. LonMaker Build Directory : %LMWBldVerRoot%
echo .. InstallShield Build Dir  : %IsProjPath%
echo .. LonMaker Install Image   : %LmInstallImage%
echo .. LonMaker Language        : %LMWLangPath%
echo .. Target Directory         : %TargetDir%
echo .. Visio Source Directory   : %VisioDir%
echo .. Visio ClickToRun Source Directory   : %VisioC2RDir%
echo .. Visio SP Directory       : %VisioServicePackDir%
echo .. Adobe Installer          : %AdobeInstaller%
echo .. Drivers Directory        : %CskDir%

if not exist "%LmInstallImage%\*.*" goto no_LM_IMAGE
if not exist "%AdobeInstaller%" goto no_ADOBE_error

rem Check if there is any existing build already in the target folder that has
rem the same number

echo Copying LonMaker version %VerString% distribution files from %LMWBldVerRoot% to %TargetDir% >>%LMWBldDir%\build.log

REM /SS1 if exist "%TargetDir%" rmdir /S /Q "%TargetDir%"

echo .
echo . Create Target Directory Structure
echo .

mkdir "%TargetDir%\Documentation\DataSheets"
mkdir "%TargetDir%\Documentation\Reader"
mkdir "%TargetDir%\IzoTCT"
mkdir "%TargetDir%\SLTA-10_Win2K-XP"

if NOT "%VisioDir%" == "" mkdir "%TargetDir%\Visio"
if NOT "%VisioC2RDir%" == "" mkdir "%TargetDir%\VisioC2R"
if NOT "%iLonVer%" == ""  mkdir "%TargetDir%\iLon"

echo .
echo *** Copying Documentation folder ***
echo .
xcopy %xcOpt% "%LMWBldVerRoot%\Doc\DataSheets\*.pdf"	"%TargetDir%\Documentation\DataSheets"
if exist "%LMWBldVerRoot%\Doc\DataSheets\%LMWLangPath%*.pdf" xcopy %xcOpt% /Y "%LMWBldVerRoot%\Doc\DataSheets\%LMWLangPath%*.pdf" "%TargetDir%\Documentation\DataSheets"

xcopy %xcOpt% "%AdobeInstaller%"					"%TargetDir%\Documentation\Reader"
echo .
echo *** Copying drivers folders ***
echo .
xcopy %xcOpt% "%CskDir%\SLTA-10 Win95-NT"			"%TargetDir%\SLTA-10_Win2K-XP" /s
echo .
echo *** Copying PostScript folders ***
echo .
xcopy %xcOpt% "%LMWBldVerRoot%\build\bin\WinSt%LMWlang%.exe"		"%TargetDir%\PostScript\" 
copy /Y "%LMWBldVerRoot%\build\bin\PostScriptReadme%LMWlang%.htm"	"%TargetDir%\PostScript\readme.htm" 

if "%VisioDir%" == "" goto noVisio
	echo .
	echo *** Copying Visio ***
	echo .
	xcopy %xcOpt% "%VisioDir%"					"%TargetDir%"\Visio /s /h /I
	xcopy %xcOpt% "%VisioC2RDir%"				"%TargetDir%"\VisioC2R /s /h /I
	xcopy %xcOpt% "%VisioupdateDir%"			"%TargetDir%"\VisioUpdate /s /h /I

:noVisio
rem
rem We install SP even if there is no Visio, since they must have Visio & we want the SP applied.
rem
if "%VisioServicePackDir%" == "" goto noVisioSP
	xcopy %xcOpt% "%VisioServicePackDir%"		"%TargetDir%\%VisioSPName%" /s /h /I
:noVisioSP

if "%iLonVer%" == "" goto noilon5 
	echo *** Copying iLon folder ***
	xcopy %xcOpt% %iLonPath%					"%TargetDir%"\iLon 
	echo .
:noilon5

echo.
echo *** Copying LonMaker files ***
echo.
xcopy %xcOpt% /S /H /I "%LmInstallImage%"		"%TargetDir%\IzoTCT"

echo *** Copying other root files ***
echo.
copy "%LMWBldVerRoot%\install\autorun.inf"								"%TargetDir%"
copy "%LMWBldVerRoot%\images\IzoTCT.ico"								"%TargetDir%"
copy "%LMWBldVerRoot%\Install\ReadMe.htm"								"%TargetDir%"
copy "%LMWBldVerRoot%\Install\license.htm"								"%TargetDir%"
copy "%LMWBldVerRoot%\build\ins\*.exe"									"%TargetDir%"
copy "%LMWBldVerRoot%\build\sys\psapi.dll"								"%TargetDir%"
copy "%LMWBldVerRoot%\build\sys\mfc*.dll"								"%TargetDir%"
copy "%LMWBldVerRoot%\build\sys\msvcr*.dll"								"%TargetDir%"
copy "%LMWBldVerRoot%\build\sys\*.manifest"								"%TargetDir%"
copy "%LMWBldVerRoot%\doc\UserGuide\OpenLNS CT User's Guide.pdf" 		"%TargetDir%\Documentation"

echo =============================================================
echo %date% %time%: LonMaker %ImageName% Installation Image Created
echo =============================================================

goto :EOF

:no_LM_IMAGE
	echo *** ERROR ***
	echo LonMaker Install Image not available on %LmInstallImage%
	echo *** ERROR *** LonMaker Install Image not available on %LmInstallImage% >>%LMWBldDir%\build.log
	pause
goto :EOF

:no_ADOBE_error
	echo *** ERROR ***
	echo Adobe reader installer not found: %AdobeInstaller%
	echo *** ERROR *** Adobe reader installer not found: %AdobeInstaller% >>%LMWBldDir%\build.log
	pause
goto :EOF
