@echo off

@echo ..
@echo ..  !!   Build the LonMaker Installer.                          !!
@echo ..

rem Follwing will be set to 0 if build succeeds.
set junkVal=1

Set IsToolsDrive=C:
Set IsBldDrive=C:
set ISProjDrive=P:
Set IsToolsPath=%IsToolsDrive%\Program Files (x86)\InstallShield\2013 SP1 SAB\System
Set IsProjDir=%ISProjDrive%\Install
Set IsProjPath=%IsProjDir%
Set IsMergeModulePath=%LMWBldVerRoot%\Import\MergeModules

Set _PATH=%PATH%
Set PATH=%PATH%;%IsToolsPath%
rem  @echo PATH="%PATH%"

rem InstallShield does not handle relative paths well, but we do not want to use absolute
rem paths in our IS scripts. Solution? Use subst to create a virtual drive that points to
rem our current project root

if exist %ISProjDrive%\* subst %ISProjDrive% /D
subst %ISProjDrive% %LMWBldVerRoot%


if "%maj%" == "" goto getversion
if "%min%" == "" goto getversion
if "%LMWbld%" == "" goto getversion
goto haveversion
:getversion
rem ********************************************
rem Get the version info from version.h
rem ********************************************

call getlnsver -f %LMWBldVerRoot%/include/version.h > ~q1.bat
call ~q1.bat > nul
del ~q1.bat
Echo ... verMajor := %VerMajor%
Echo ... verMinor := %VerMinor%
Echo ... verBuild := %VerBuild%
Echo .
set maj=%VerMajor%
set min=%VerMinor%
set LMWbld=%VerBuild%

:haveversion

rem  Set the Product Version string for use throughout
Set ProductVersion=%maj%.%min%.%LMWbld%

rem ..
rem ..  The InstallShield Tools directory and project directory must
rem ..  have been previously set up.  The Tools directory is where the
rem ..  InstallShield 2013 standalone build tools have been
rem ..  installed.  The Project Directory should have been previously
rem ..  been set up by the main batch file in the build script,
rem ..  which will get all of the files for the installation, time and
rem ..  date stamp them, and put them in the appropriate directory tree
rem ..  as components for building the LonMaker installers.  It
rem ..  should also get the latest InstallShield project files from
rem ..  source control.
rem ..

rem  The InstallShield project files will be gotten from source control by
rem  the global update for the entire project.  We will remove the read-only
rem  attribute, because their versions will be modified during this process, but
rem  we do not want to check in new versions, because they may be checked out for
rem  development.

%ISProjDrive%
CD %IsProjPath%

@echo ----------------------------------------
@echo  ***** Build the Product Installs *****
@echo ----------------------------------------

For %%d In (CtXmlMsi LonMaker_Silent) Do Call :BuildInstallComponent %%d "Product Install"

copy "%IsProjPath%\CtXmlMsi\CtXmlMsi\Release\MSI\DiskImages\DISK1\*.msi" 			%LMWBldVerRoot%\build\SilentInstallers\
copy "%IsProjPath%\LonMaker_Silent\LonMaker_Silent\Release\DiskImages\DISK1\*.msi" 	%LMWBldVerRoot%\build\SilentInstallers\

%ISProjDrive%
CD %IsProjPath%

For %%d In (LonMaker) Do Call :BuildInstallComponent %%d "Product Install"
If ErrorLevel 1 Goto done_error

goto done_noerror


:done_error
Echo ...
Echo ... LonMaker generated a real error.  Look at Installer build .log files for cause
Echo ...
Echo .

Goto End


:done_noerror

CD %IsProjDir%
set junkVal=0

Goto End


Rem     --- BEGIN SUBROUTINES --------------------------------------------------------

Rem	Subroutine:  BuildInstallComponent
Rem     Subroutine to build the InstallShield component
Rem      arg1 - The project name
Rem	     arg2 - Component Type
:BuildInstallComponent
@echo -------------------------------------------
@echo   Build the %1 %2
@echo -------------------------------------------

cd %1
if EXIST %1.ism.doublecheck erase %1.ism.doublecheck
copy %1.ism %1.ism.backup
attrib -r  %1.ism

Rem Set new Product code with each build, so that each is a major upgrade
echo cscript //nologo ..\SetNewMsiProductCodeIs2013.vbs %1.ism
cscript //nologo ..\SetNewMsiProductCodeIs2013.vbs %1.ism

Rem Set the version number in the Upgrade Table, to identify higher versions
echo Call SetInstallShieldUpgradeVer "%IsProjPath%\%1\%1.ism" %ProductVersion%
Call SetInstallShieldUpgradeVer "%IsProjPath%\%1\%1.ism" %ProductVersion%

If ErrorLevel 1 Goto done_error

rem Build the Component via the InstallShield standalone tools
echo ISCmdBld.exe -p %1.ism -x -o %IsMergeModulePath% -y %ProductVersion% 
ISCmdBld.exe -p %1.ism -x -o %IsMergeModulePath% -y %ProductVersion% 
If ErrorLevel 1 Goto done_error

Rem Restore the InstallShield project file to its former values
rename %1.ism %1.ism.doublecheck
rename %1.ism.backup %1.ism
attrib +r  %1.ism

cd ..

Goto :EOF

Rem     --- END SUBROUTINES ----------------------------------------------------------


:End

rem Set path back to initial value
Set PATH=%_PATH%

@echo ..  End of InstallShield 2013 project builds


