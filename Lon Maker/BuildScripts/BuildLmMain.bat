@echo off
REM BUILDS51.BAT for PC -
REM program to build all product files for LonMaker For Windows (BUILDSYS)
REM using InstallShield 5.1

if "%1" == "help" goto help
goto start
:help
echo.
echo buildlmw [n] [-noclean] [-noupdate] [-noexport] [-demo] [-l XXX] [-v xx] [-s SP]
echo    where n is the build number
echo          -noclean  : don't clear out old output files
echo          -noupdate : don't get the latest version from Perforce, 
echo          -demo     : create demo version image
echo	      -nostd    : do not create the Standard Edition image
echo	      -l XXX    : specify the 3-letter MS language abbreviation	
echo	      -v xx     : specify the version number for iLon
echo          -batch    : bypass any user confirmation
echo	      -noexport : Do not export LMW files
echo	      -s        : SP to specify a Service Pack build, where SP is the forked baseline name (eg LMW31x)
echo
goto end
:start
Echo -------------------------------------------------------------
Echo -
echo Program to build the product files for LonMaker For Windows
echo .
echo Make sure that the following folders are on the path
echo		- Bin folder
echo		- Source Save
echo		- Install Shield
echo		- MSDev\Bin
echo .
echo You must have already archived the previous build.
Echo -
Echo -------------------------------------------------------------

echo .
echo .
Rem --------------------------------------------------------
Rem     Setup environment variables used in this script to
Rem     avoid excess replication, and to ease changes.
Rem     Set tracing variables

Rem Set NUL=NUL
Set NUL=CON

Rem     Set command-lines used by this script.


Set NUL=CON

Rem     Root of all LMW source files under source control. (Final baseLine is set 
Rem	below after parsing args)
Rem

Set SSLPRoot=//depot/Software/NetTools/LonPoint/Dev
Set SSRedistRoot=%git_component%
Set SSExportRoot=%git_export%

Set RedistDir=%git_component%
Set FinalTargetDir=\\Virgil\Shared SW\LonMaker Installations\Internal
Set PerlDir=\Perl

rem NOTE: As of Visio 2010, there is a single installer for all flavors. So we point all to the same place.
rem
Set VisioProDir=%git_component%Others\Microsoft\Visio\Visio2016\Visio 2016 Professional Volume Licensing Edition
Set VisioProC2RDir=%git_component%Others\Microsoft\Visio\Visio2016\Visio 2016 Professional C2R VLE
Set VisioStdDir=%git_component%Others\Microsoft\Visio\Visio2016\Visio 2016 Standard Volume Licensing Edition
Set VisioStdC2RDir=%git_component%Others\Microsoft\Visio\Visio2016\Visio 2016 Standard C2R VLE
Set VisioDemoDir=%git_component%Others\Microsoft\Visio\Visio2016\Visio 2016 Standard Volume Licensing Edition
Set VisioDemoC2RDir=%git_component%Others\Microsoft\Visio\Visio2016\Visio 2016 Standard C2R VLE
Set VisioServicePackDir=
Set VisioSPName=Visio SP3
Set iLonDir=\\Ntengr\Shared SW\iLon Beta\iLon 1.00.
Set ExportDir=%git_export%

rem Set LNSUpdate1Dir=

Set TargetInternalDir=
set iLonVer=


:prodok1

REM process the switches
set LMWbld=
set nolib=
set rebuild=/Build
set exp=Y
set demo=N
set LMWlang=ENU
set LMWLangPath=
set iLonPath=
set iLonInfo=

:startargs
if "%1" == "" goto endargs
if not "%1" == "-noclean" goto arg2
goto nextarg
:arg2
if not "%1" == "-noupdate" goto arg3
goto nextarg
:arg3
if not "%1" == "-demo" goto arg6
set demo=Y
goto nextarg
:arg6
if not "%1" == "-l" goto arg8
shift
set LMWlang=%1%
set noclean=noclean
echo "Setting -noclean option for language specific build."
goto nextarg
:arg8
if not "%1" == "-v" goto arg10
shift
set iLonVer=%1%
goto nextarg
:arg10
if not "%1" == "-nostd" goto arg14
goto nextarg
:arg14
if not "%1" == "-noexport" goto arg15
set exp=N
goto nextarg
:arg15

set LMWbld=%1%
:nextarg
shift

goto startargs
:endargs

Set VisioProDir=%VisioProDir%\%LMWlang%
Set VisioProC2RDir=%VisioProC2RDir%\%LMWlang%
Set VisioStdDir=%VisioStdDir%\%LMWlang%
Set VisioStdC2RDir=%VisioStdC2RDir%\%LMWlang%
Set VisioDemoDir=%VisioDemoDir%\%LMWlang%
Set VisioDemoC2RDir=%VisioDemoC2RDir%\%LMWlang%
set adobeReader=AdbeRdr920_%LMWlang%.exe
set LNSRedistDir=%LMWBldDir%\dev\Import\Installers
set MPRDir=%LMWBldDir%\dev\Import\MPR
Set CskDir=\\Homer\Home6\CSK SW

REM
REM First update the scripts that are in pf1
REM NOTE: Even though PF1 tools are the same for every build tree, we must have a 
REM		unique workspace name for each build tree since the builds are done on
REM		different PCs
REM


echo ..
echo .. Building from %SSLMWVerRoot%
echo ..
echo .. LNS Redist : %LNSRedistDir%
echo .. MPR Router : %MPRDir%
echo .. Visio Pro  : %VisioProDir%
echo .. Visio Pro ClickToRun  : %VisioProC2RDir%
echo .. Visio Std  : %VisioStdDir%
echo .. Visio Std ClickToRun : %VisioStdC2RDir%
echo .. Visio Demo : %VisioDemoDir%
echo .. Visio Demo ClickToRun: %VisioDemoC2RDir%

if not exist "%LNSRedistDir%\*.*"		goto noLNS
if not exist "%MPRDir%"	                goto noMPR
if not exist "%VisioProDir%"			goto err3a
if not exist "%VisioProC2RDir%"			goto err3aa
if not exist "%VisioStdDir%"			goto err3c
if not exist "%VisioStdC2RDir%"			goto err3cc
if "%demo%" == "N" goto nodemo
	if not exist "%VisioDemoDir%"		goto err3b
	if not exist "%VisioDemoC2RDir%"	goto err3bb
:nodemo


set now=%date% %time%

echo "=======================================================" >>%logdir%\build.log
echo "%date% %time%: Starting LonMaker Build >>%logdir%\build.log
echo "=======================================================" >>%logdir%\build.log

if "%iLonVer%" == "" goto noilon 
	set iLonPath="%iLonDir%%iLonVer%\PC Setup"
	if not exist %iLonPath% goto erriLON
:noilon

if "%LMWlang%" == "ENU" goto nextcmd
set LMWLangPath=L.%LMWlang%\


:nextcmd

rem ********************************************
echo Getting the version info from version.h >> %logdir%\build.log
echo .
rem ********************************************
echo ..
echo .. Compute build number
echo ..
REM
REM Run SetVersion once to just get build #. Re-run again (after we check out the file)
REM to actually update the version file
REM
BuildTools\SetVersion -e -r "%LMWBldVerRoot%\Include\version.h" > ~q1.bat
call ~q1.bat > nul
type ~q1.bat >> "%logdir%\build.log"
del ~q1.bat

set maj=%VerMajor%
set min=%VerMinor%

rem %VerString% contains version # in format M.mm.bb

if not "%LMWbld%" == "" goto use_LMW_ver
	rem *
	rem * If build number is not specified, use the version saved in version.h
	rem * If build number is specified from the command line, update the version.h in LON project
	rem *
	echo ... Increment build number from version.h (%VerBuild%) ...
	set LMWbld=%VerBuild%
goto ver_assigned

:use_LMW_ver
echo .
echo ... Use the specified build version (%LMWbld%)...  
echo .

:ver_assigned
echo .

BuildTools\SetVersion -e "%LMWBldVerRoot%\Include\version.h" > ~q1.bat
call ~q1.bat > nul
type ~q1.bat >> "%logdir%\build.log"
del ~q1.bat

echo ..
echo . Checkout Exported IzoT CT XML Utility merge module
echo ..
echo.
echo Creating build %LMWbld% for release %VerString%
echo Creating build %LMWbld% for release %VerString% >>%logdir%\build.log
if "%iLonVer%" == "  " goto noilon1 
set iLonInfo=iLon:%iLonVer%
goto dispOption
:noilon1
set iLonInfo=
:dispOption
echo Options supplied: %noclean% %nolib% %upd% %LMWlang% %iLonInfo%
echo Options supplied: %noclean% %nolib% %upd% %LMWlang% %iLonInfo% >>%logdir%\build.log

echo.
echo If these conditions are not correct, type Ctrl-C and correct them, else

REM Delete and recreate build directories and clear out product directory
REM /SS1 if exist "%LMWBldVerRoot%\build" rmdir /S /Q "%LMWBldVerRoot%\build"
goto makedir

:skipcln1
if exist %LMWBldVerRoot%\build goto update1

:makedir

mkdir %LMWBldVerRoot%\build
mkdir %LMWBldVerRoot%\build\bin
mkdir %LMWBldVerRoot%\build\doc
mkdir %LMWBldVerRoot%\build\doc\German
mkdir %LMWBldVerRoot%\build\help
mkdir %LMWBldVerRoot%\build\ins
mkdir %LMWBldVerRoot%\build\lnshelperdlls
mkdir %LMWBldVerRoot%\build\lnsredist
mkdir "%LMWBldVerRoot%\build\lnsredist\LNS Server"
mkdir "%LMWBldVerRoot%\build\lnsredist\LNS Updates"
mkdir "%LMWBldVerRoot%\build\lnsredist\License Ocx"
mkdir %LMWBldVerRoot%\build\sys
mkdir %LMWBldVerRoot%\build\sysreg
mkdir %LMWBldVerRoot%\build\types
mkdir %LMWBldVerRoot%\build\vst
mkdir %LMWBldVerRoot%\build\XML
mkdir %LMWBldVerRoot%\build\MPR
mkdir %LMWBldVerRoot%\build\custom\Echelon
mkdir %LMWBldVerRoot%\build\SilentInstallers

rem mkdir "%LMWBldVerRoot%\build\%LMWLangPath%bin"
rem mkdir "%LMWBldVerRoot%\build\%LMWLangPath%help"
rem mkdir "%LMWBldVerRoot%\build\%LMWLangPath%vst"

:update1

if "%noclean%" == "noclean" goto skipcln
echo Cleaning all the local objects and libraries
echo .
set rebuild=/Rebuild
:skipcln

if exist mak\release\*.* echo Clearing contents of mak\release
if exist mak\release\*.* del /Q mak\release\*.*

if exist mak\debug\*.* echo Clearing contents of mak\debug
if exist mak\debug\*.* del /Q mak\debug\*.*


set BaseTargetDir=%FinalTargetDir%\System Test\%VerString%
echo Check for existance of %BaseTargetDir%
if NOT exist "%BaseTargetDir%" echo Creating %BaseTargetDir%
if NOT exist "%BaseTargetDir%" mkdir "%BaseTargetDir%"
set BaseTargetDir=%BaseTargetDir%\%LMWlang%
echo Check for existance of %BaseTargetDir%
if NOT exist "%BaseTargetDir%" echo Creating %BaseTargetDir%
if NOT exist "%BaseTargetDir%" mkdir "%BaseTargetDir%"

set TargetInternalDir=%FinalTargetDir%\Development\%VerString%



:dobuild
echo =============================================================
echo %date% %time%: Building the project libraries and exe
echo %date% %time%: Building the project libraries and exe >>%logdir%\build.log
echo =============================================================

rem For language specific builds, we ONLY build the resource files.
set buildWs=all
if "%LMWlang%" == "ENU" goto startBuild
set buildws=all%LMWlang%

:startBuild

if "%noclean%" == "noclean" goto skipsdk
echo Install Microsft SDK Include directories
REM /SS1 "C:\Program Files\Microsoft SDK\Setup\VCIntegrate.exe" /I "C:\Program Files\Microsoft SDK\"
:skipsdk

Set start_build_comment="start LMW build %LMWbld% (%buildws%)"
Set end_build_comment="end LMW build %LMWbld% (%buildws%)"

echo call devenv %LMWBldVerRoot%\all\%buildws%.sln %rebuild% Release /Out %logdir%\buildlmw.log
call devenv %LMWBldVerRoot%\all\%buildws%.sln %rebuild% Release /Out %logdir%\buildlmw.log
if errorlevel 1 goto doneLMWerror

:buildLmwIntfc
echo "Skipping build of debug version of lmwintfc.dll"
goto buildPid
echo =============================================================
echo %date% %time%: Building the debug DLLs (LMWLCA32 and LMWINTFC) 
echo %date% %time%: Building the debug DLLs (LMWLCA32 and LMWINTFC) >>%logdir%\build.log
echo =============================================================

echo call devenv %LMWBldVerRoot%\NB\lmwintfc.sln %rebuild% Debug /Out %logdir%\builddbg.log
call devenv %LMWBldVerRoot%\NB\lmwintfc.sln %rebuild% Debug /Out %logdir%\builddbg.log
if errorlevel 1 goto doneLMWerror

:buildPid

echo =============================================================
echo %date% %time%: Building NewPid 
echo %date% %time%: Building NewPid >>%logdir%\build.log
echo =============================================================

call devenv %LMWBldVerRoot%\newpid\unmark.dsp /make "all - Win32 Release" %rebuild% /out %logdir%\buildpid.log
if errorlevel 1 goto done_error

echo =============================================================
echo %date% %time%: Done building LonMaker ....
echo =============================================================


rem ********************************************
echo Copy DLLs and EXEs into build folder >>%logdir%\build.log
rem ********************************************
cd %LMWBldVerRoot%
rem @echo on
copy mak\release\lmw*.dll 				build\bin
copy mak\release\lmwact32.ocx 			build\bin
copy mak\release\IzoTCTSetup.exe 		build\ins
copy mak\release\lmwbrw32.exe 			build\bin
copy mak\release\lmXml.exe	  			build\bin
copy mak\release\IzoTCTDesignMgr.exe 	build\bin
copy mak\release\IzoTCT.vsl 			build\bin
rem
rem Copy reg files to be used by Installer
rem
copy brw\lmwbrw32.reg 					build\bin
copy lmXml\LmXml.reg  					build\bin

copy win32util\data\*.xml build\types

set dllVer=%VerMajor%00
rem *********************************************
echo Sign LonMaker executables with Digital Signature (DLL Version: %dllVer%)
rem *********************************************

set signCodeParams=-cn "Echelon Corporation" -a SHA1 -t http://timestamp.verisign.com/scripts/timstamp.dll
signcode %signCodeParams% .\build\bin\IzoTCTDesignMgr.exe
signcode %signCodeParams% .\build\bin\IzoTCT.vsl
signcode %signCodeParams% .\build\bin\lmwbrw32.exe
signcode %signCodeParams% .\build\bin\lmXml.exe
signcode %signCodeParams% .\build\ins\IzoTCTSetup.exe
signcode %signCodeParams% .\build\bin\lmwact32.ocx
signcode %signCodeParams% .\build\bin\LmwLca%dllVer%.dll
signcode %signCodeParams% .\build\bin\LmwNet%dllVer%.dll
signcode %signCodeParams% .\build\bin\LmwCct%dllVer%.dll
signcode %signCodeParams% .\build\bin\LmwCha%dllVer%.dll
signcode %signCodeParams% .\build\bin\LmwMan%dllVer%.dll
signcode %signCodeParams% .\build\bin\LmwNdw%dllVer%.dll
signcode %signCodeParams% .\build\bin\LmwSub%dllVer%.dll
signcode %signCodeParams% .\build\bin\LmwUsr%dllVer%.dll

if "%LMWlang%" == "ENU" goto noLNSLang

copy mak\release\LmwSetup%LMWlang%.dll build\ins
copy LNS\%LMWLangPath%*.* build\bin

:noLNSLang

echo ..
echo .. Begin copy of build files
echo ..

cd %LMWBldVerRoot%

rem ********************************************
echo Copy icon, bitmap, ReadMe >>%logdir%\build.log
rem ********************************************
copy Images\IzoTCT.ico 						build\bin
copy Images\499x312_IzoT_CT_Installer.bmp 	build\bin
copy Images\499x58_IzoT_CT_Banner.bmp		build\bin
copy Install\ReadMe.htm 					build\bin

rem ********************************************
echo Copy INS files >>%logdir%\build.log
rem ********************************************
copy %LMWLangPath%install\IzoTCT.url		build\%LMWLangPath%bin
copy %LMWLangPath%install\license.txt		build\%LMWLangPath%bin
copy %LMWLangPath%install\license.htm		build\%LMWLangPath%bin
copy %LMWLangPath%install\license.rtf		build\%LMWLangPath%bin

rem ********************************************
echo Copy VST files >>%logdir%\build.log
rem ********************************************
copy vst\%LMWLangPath%*.vss    				build\%LMWLangPath%vst
copy vst\%LMWLangPath%*.vst    				build\%LMWLangPath%vst
if errorlevel 1 goto done_error

rem ********************************************
echo Copy Echelon Code Signing Certificate files >>%logdir%\build.log
rem ********************************************
copy vst\Echelon.cer    					build\custom\Echelon
if errorlevel 1 goto done_error

rem ********************************************
echo Copy documentation files >>%logdir%\build.log
rem ********************************************
copy "doc\UserGuide\OpenLNS CT User's Guide.pdf"	    build\doc
copy doc\UserGuide\LonWorks.pdf    						build\doc
rem copy doc\UserGuide\LonMaker.pdf    		    		build\doc\German
if errorlevel 1 goto done_error

rem ********************************************
echo Copy XML Folder >>%logdir%\build.log
rem ********************************************
copy LonWorks\LonMaker\XML\*.*    		    build\XML
if errorlevel 1 goto done_error

rem ********************************************
echo Copy NodeBuilder dll >>%logdir%\build.log
rem ********************************************
copy LonWorks\Bin\lmwlca32.dll    		    build\bin
if errorlevel 1 goto done_error

rem ********************************************
echo Copy CT-Visio helper exes >>%logdir%\build.log
rem ********************************************
copy %LMWBldDir%\dev\Import\Installers\CTRegSvc.exe		build\bin
copy %LMWBldDir%\dev\Import\Installers\VslSoftlink.exe 	build\bin
if errorlevel 1 goto done_error

rem ********************************************
echo Copy LNS helper dlls >>%logdir%\build.log
rem ********************************************
copy %LMWBldDir%\dev\Import\Installers\*.dll	build\lnshelperdlls
if errorlevel 1 goto done_error

rem ********************************************
echo Copy LNS Server redistribution >>%logdir%\build.log
rem ********************************************
copy "%LNSRedistDir%\*.msi"                 "build\lnsredist\LNS Server"
copy "%LNSRedistDir%\LicenseWizard100.exe"  "build\lnsredist\LNS Server"
if errorlevel 1 goto done_error

rem ********************************************
echo Copy MPR Installation >>%logdir%\build.log
rem ********************************************
copy "%MPRDir%\*.msi"                       "build\MPR"
copy "%MPRDir%\LonMaker\MPR-50 Multi-Port Router.vss"	"build\MPR\Multi-PortRouter.vss"
copy "%MPRDir%\MprMn\mprmn.xif"             "build\MPR"
copy "%MPRDir%\MprMn\mprmn.xfb"             "build\MPR"
if errorlevel 1 goto done_error

rem ********************************************
echo Copy SilentInstallers >>%logdir%\build.log
rem ********************************************
copy "%LMWBldVerRoot%\Import\Installers\LicenseWizard100.msi"   		"build\SilentInstallers"
copy "%LMWBldVerRoot%\Import\Installers\msxml6.msi"   					"build\SilentInstallers"
copy "%LMWBldVerRoot%\Import\Installers\Ip852ConfigServer401.msi"   	"build\SilentInstallers"
copy "%LMWBldVerRoot%\Import\Installers\LonMarkResourceFiles1600.msi"   "build\SilentInstallers"
copy "%LMWBldVerRoot%\Import\Installers\OpenLDV510.msi"   				"build\SilentInstallers"
copy "%LMWBldVerRoot%\Import\Installers\IzoTNetServerSilent440.msi"   	"build\SilentInstallers"
copy "%LMWBldVerRoot%\Install\Silent.bat"   							"build\SilentInstallers"

if errorlevel 1 goto done_error

rem ********************************************
echo Copy HELP files >>%logdir%\build.log
rem ********************************************
copy hlp\*.hlp								build\%LMWLangPath%help
copy hlp\*.cnt								build\%LMWLangPath%help
copy hlp\*.chm								build\%LMWLangPath%help
copy /Y hlp\%LMWLangPath%*.hlp				build\%LMWLangPath%help
copy /Y hlp\%LMWLangPath%*.cnt				build\%LMWLangPath%help	
copy /Y hlp\%LMWLangPath%*.chm				build\%LMWLangPath%help
copy newpid\release\newpid.exe				build\%LMWLangPath%bin
copy newpid\newpid.hlp						build\%LMWLangPath%bin
copy newpid\newpid.cnt						build\%LMWLangPath%bin
copy key\LMWxfer.exe						build\%LMWLangPath%bin
copy key\LmwLicWz.exe						build\%LMWLangPath%bin

copy %RedistDir%\Others\Microsoft\psapi.dll							build\sys
copy %RedistDir%\Others\Microsoft\comppl32.dll						build\sys
rem copy this file to the bin folder as its required for self-registration of exes
copy build\sys\comppl32.dll											build\bin
copy %RedistDir%\Others\Microsoft\mfc120.dll						build\sys
copy %RedistDir%\Others\Microsoft\Microsoft.VC120.MFC.manifest		build\sys
copy %RedistDir%\Others\Microsoft\msvcr120.dll						build\sys
copy %RedistDir%\Others\Microsoft\Microsoft.VC120.CRT.manifest		build\sys
copy %RedistDir%\Others\Microsoft\msvbvm50.dll						build\sysreg
copy %RedistDir%\Others\Microsoft\comdlg32.ocx						build\sysreg
copy %RedistDir%\Others\Microsoft\msflxgrd.ocx						build\sysreg
copy %RedistDir%\Others\Microsoft\INETWH32.dll						build\sys

attrib -r build\sysreg\*.*
attrib -r build\sys\*.*

echo **** Setting the correct date and time stamp ****
setfdate */*/* %maj%:%min%:0 build\bin\*.*		>>%logdir%\build.log
rem setfdate */*/* %maj%:%min%:0 build\exereg\*.*	>>%logdir%\build.log
setfdate */*/* %maj%:%min%:0 build\binreg\*.*		>>%logdir%\build.log
setfdate */*/* %maj%:%min%:0 build\ins\*.*			>>%logdir%\build.log
setfdate */*/* %maj%:%min%:0 build\help\*.*			>>%logdir%\build.log
setfdate */*/* %maj%:%min%:0 build\include\*.*		>>%logdir%\build.log
setfdate */*/* %maj%:%min%:0 build\help\*.*			>>%logdir%\build.log
setfdate */*/* %maj%:%min%:0 build\drivers\*.*		>>%logdir%\build.log
setfdate */*/* %maj%:%min%:0 build\vst\*.*			>>%logdir%\build.log
setfdate */*/* %maj%:%min%:0 build\doc\*.*			>>%logdir%\build.log
setfdate */*/* %maj%:%min%:0 build\XML\*.*			>>%logdir%\build.log
rem setfdate */*/* %maj%:%min%:0 build\bin\lnsutil.hlp	>>%logdir%\build.log

if "%LMWLangPath%" == "" goto nolangsetfd

setfdate */*/* %maj%:%min%:0 build\%LMWLangPath%bin\*.*		>>%logdir%\build.log
setfdate */*/* %maj%:%min%:0 build\%LMWLangPath%help\*.*	>>%logdir%\build.log
setfdate */*/* %maj%:%min%:0 build\%LMWLangPath%vst\*.*		>>%logdir%\build.log

:nolangsetfd

if exist \lon goto copylon
echo .. Create lon folder ...
md \lon
:copylon
echo .. Copying files to build folder
setfdate @\lon\redist.lst build\sys					>>%logdir%\build.log
setfdate @\lon\redist.lst build\sysreg				>>%logdir%\build.log
setfdate @\lon\dateldrf.lst build\types				>>%logdir%\build.log
setfdate @\lon\dateldrf.lst build\binreg			>>%logdir%\build.log
setfdate @\lon\datemisc.lst build\types				>>%logdir%\build.log
setfdate @\lon\datemisc.lst build\binreg			>>%logdir%\build.log
setfdate @\lon\bin.lst build\bin					>>%logdir%\build.log

rem ********************************************
echo Getting the latest Adobe Acrobat Reader (%RedistDir%\Others\Adobe\Reader\%adobeReader%) >> %logdir%\build.log
rem ********************************************

echo copy %RedistDir%\Others\Adobe\Reader\%adobeReader% 	build\bin
copy %RedistDir%\Others\Adobe\Reader\%adobeReader% 			build\bin

rem ********************************************
echo Getting the latest PostScript Driver >> %logdir%\build.log
rem ********************************************
copy %RedistDir%\Others\Adobe\PostScriptDriver\WinSt%LMWlang%.exe				build\bin
copy %RedistDir%\Others\Adobe\PostScriptDriver\PostScriptReadme%LMWlang%.htm	build\bin

echo =============================================================
echo %date% %time%: Building installshield projects 
echo %date% %time%: Building installshield projects >> %logdir%\build.log
echo =============================================================

call %LMWBldVerRoot%\BuildScripts\BuildLmwInstallers.bat
if not "%junkVal%" == "0" goto build_install_err

echo =============================================================
echo %date% %time%: Export IzoT CT XML Utility Merge Module 
echo %date% %time%: Export IzoT CT XML Utility Merge Module  >> %logdir%\build.log
echo =============================================================

copy /Y "%LMWBldVerRoot%\Install\CtXmlMsi\CtXmlMsi\Release\MSI\DiskImages\DISK1\Echelon IzoT CT XML Utility.msi" "%ExportDir%\LnsCtXML\v4.0x\Install\Echelon IzoT CT XML Utility.msi"

:doLabel
rem ********************************************
rem * label the base line with each 
rem * build number that is performed
rem ********************************************
set label=LMBuild_%VerString%
echo =============================================================
echo %date% %time%:  Labeling %SSLMWVerRoot% as "%label%"
echo =============================================================
rem
rem Note the -f defaults to Yes to remove duplicate label
rem 

echo %msdrive%
%msdrive%
echo Current Directory is  : %cd%
cd %git_lonmaker%

echo command is  git add "%LMWBldVerRoot%\Include\version.h"
git add "%LMWBldVerRoot%\INCLUDE\version.h"
git commit -m "Version edits for LonMaker Build %VerString%"
echo command is git push origin %git_lonmaker_branch%
git push origin %git_lonmaker_branch%



cd %git_export%
echo command is git add "%SSExportRoot%\LnsCtXML\v4.0x\Install\Echelon IzoT CT XML Utility.msi"
git add "%SSExportRoot%\LnsCtXML\v4.0x\Install\Echelon IzoT CT XML Utility.msi"
git commit -m "Version edits for LonMaker Build %VerString%"
echo command is git push origin %git_export_branch%
git push origin %git_export_branch%
	


echo ... done labeling the baseline ....
echo.


rem echo %msdrive%
rem %msdrive%

:ExportFiles
if "%exp%" == "N" goto SkipExport
echo Call %toolsBin%\GIT_Import_Export.bat %LMWBldVerRoot%\BuildViews\%P4LonMakerExportBranch%.txt "LonMaker Build %VerString% exports. Exported file from project located at %git_lonmaker% of the branch %git_lonmaker_branch% on host '%COMPUTERNAME%'. View file used:%P4LonMakerExportBranch%"
Call %toolsBin%\GIT_Import_Export.bat %LMWBldVerRoot%\BuildViews\%P4LonMakerExportBranch%.txt "LonMaker Build %VerString% exports. Exported file from project located at '%git_lonmaker%' of the branch '%git_lonmaker_branch%' on host '%COMPUTERNAME%'. View file used:%P4LonMakerExportBranch%"
If ErrorLevel 1 Goto done_error
:SkipExport


:skiplp
cd %LMWBldDir%
echo =============================================================
echo %date% %time%: Please examine the results files for any build problems.
echo End of system build
echo =============================================================

rem execute this batch file for user defined clean-up, mail, etc.
if exist %LMWBldVerRoot%\pcb\sbldfini.bat %LMWBldVerRoot%\pcb\sbldfini.bat


rem if not exist "%LMWBldVerRoot%\product\Disk Images\disk1"\data1.cab goto done_error


echo =============================================================
echo %date% %time%: Copying LonMaker version %VerString% distribution files from build machine to %FinalTargetDir%
echo %date% %time%: Copying LonMaker version %VerString% distribution files from build machine to %FinalTargetDir% >>%logdir%\build.log
echo =============================================================
echo .

echo .
echo *** Seting up folder for Internal use ***
echo Check for existance of %BaseTargetDir%
if NOT exist "%TargetInternalDir%" echo Creating %TargetInternalDir%
if NOT exist "%TargetInternalDir%" mkdir "%TargetInternalDir%"
mkdir "%TargetInternalDir%\PDB"
mkdir "%TargetInternalDir%\MAP"
echo .
echo *** Copying MAP and PDB files ***
echo.
copy %LMWBldVerRoot%\mak\release\*.pdb "%TargetInternalDir%\PDB"
copy %LMWBldVerRoot%\mak\release\*.map "%TargetInternalDir%\MAP"

rem *******************************************************************************************
echo Setting up IzoT CT Product Images >> %logdir%\build.log
rem *******************************************************************************************
rem
rem We always create the Professional, Standard, and Professional/No Visio images. Demo is optional
rem

call %LMWBldVerRoot%/BuildScripts/CreateLmwInstallImage Professional				"%BaseTargetDir%\Pro"			"%VisioProDir%"		"%VisioProC2RDir%"
call %LMWBldVerRoot%/BuildScripts/CreateLmwInstallImage Standard					"%BaseTargetDir%\Standard"		"%VisioStdDir%"		"%VisioStdC2RDir%"
call %LMWBldVerRoot%/BuildScripts/CreateLmwInstallImage "Professional/No Visio"		"%BaseTargetDir%\ProNoVisio"	""					""
if "%demo%" == "N" goto skipdemo
	call %LMWBldVerRoot%/BuildScripts/CreateLmwInstallImage "Trial Edition"			"%BaseTargetDir%\Trial"			"%VisioDemoDir%"	"%VisioDemoC2RDir%"
:skipdemo

rem *******************************************************************************************
echo Setting up IzoT CT XML Utility Installer >> %logdir%\build.log
echo =============================================================
echo Setting up IzoT CT XML Utility Installer 
echo =============================================================
rem *******************************************************************************************
mkdir "%BaseTargetDir%\IzoTCtXml"
copy "%IsProjPath%\CtXmlMsi\CtXmlMsi\Release\MSI\DiskImages\DISK1\*.msi" "%BaseTargetDir%\IzoTCtXml"

set maj=
set min=
set LMWbld=
echo =============================================================
echo %date% %time% LonMaker Build %VerString% complete 
echo %date% %time% LonMaker Build %VerString% complete >>%logdir%\build.log
echo =============================================================
goto realend

rem *******************************************************************************************
rem *******							Error branches										*******
rem *******************************************************************************************

:noLNS
echo.
echo *** ERROR *** Cannot build - Missing %LNSRedistDir% folder
echo *** ERROR *** Cannot build - Missing %LNSRedistDir% folder >>%logdir%\build.log
goto revert

:noMPR
echo.
echo *** ERROR *** Cannot build - Missing %MPRDir% folder
echo *** ERROR *** Cannot build - Missing %MPRDir% folder >>%logdir%\build.log
goto revert

:err3a
echo.
echo *** ERROR *** Cannot copy - Missing %VisioProDir% folder
echo *** ERROR *** Cannot copy - Missing %VisioProDir% folder >>%logdir%\build.log
goto revert
:err3aa
echo.
echo *** ERROR *** Cannot copy - Missing %VisioProC2RDir% folder
echo *** ERROR *** Cannot copy - Missing %VisioProC2RDir% folder >>%logdir%\build.log
goto revert
:err3b
echo.
echo *** ERROR *** Cannot copy - Missing %VisioDemoDir% folder
echo *** ERROR *** Cannot copy - Missing %VisioDemoDir%  folder >>%logdir%\build.log
goto revert
:err3bb
echo.
echo *** ERROR *** Cannot copy - Missing %VisioDemoC2RDir% folder
echo *** ERROR *** Cannot copy - Missing %VisioDemoC2RDir%  folder >>%logdir%\build.log
goto revert
:err3c
echo.
echo *** ERROR *** Cannot copy - Missing %VisioStdDir% folder
echo *** ERROR *** Cannot copy - Missing %VisioStdDir%  folder >>%logdir%\build.log
goto revert
:err3cc
echo.
echo *** ERROR *** Cannot copy - Missing %VisioStdC2RDir% folder
echo *** ERROR *** Cannot copy - Missing %VisioStdC2RDir%  folder >>%logdir%\build.log
goto revert

:erriLON
echo.
echo *** ERROR *** Cannot copy - Missing %iLonPath% folder
echo *** ERROR *** Cannot copy - Missing %iLonPath% folder >>%logdir%\build.log
goto revert

:doneLMWerror

goto done_error

:build_install_err
echo ....
echo *** ERROR *** Creation of installation image failed.  Check %logdir%\buildInstall.log 
echo *** ERROR *** Creation of installation image failed. >> %logdir%\build.log 
echo ....
goto revert

:done_error
echo ....
echo *** ERROR *** something created a real error (skipped warnings).  Check %logdir%\build.log 
echo *** ERROR *** something created a real error (skipped warnings) >> %logdir%\build.log 
echo ....
:revert
cd %LMWBldDir%
REM /SS if "%VersionChangelist%" == "" goto norevert
REM /SS 	echo Reverting all pending changes in change list %VersionChangelist%
REM /SS 	p4 revert -c %VersionChangelist% //depot/...
REM /SS 	p4 change -d %VersionChangelist%
:norevert
:realend

