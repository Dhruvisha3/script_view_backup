@echo off
SETLOCAL ENABLEDELAYEDEXPANSION

REM
REM This is just a shell to update the build tools, then invoke the main build tool DcxBuildMain.bat.
REM

set argList=%1 %2 %3 %4 %5 %6 %7 %8 %9
set noclean=
set upd=
set p4SyncSwitch=

set BldDrive=C:
set git_projects=\GitTools
set git_lonmaker=%BldDrive%%git_projects%\lonmaker\
set git_export=%BldDrive%%git_projects%\export\
set git_component=%BldDrive%%git_projects%\components\

set git_lonmaker_branch=Dev
set git_export_branch=Dev
set git_component_branch=Dev

set LMWBldDir=%cd%
set lmroot=LonMaker
set SSLMWRoot=%BldDrive%%git_projects%\%lmroot%
set toolsBin=%BldDrive%%git_projects%\tools\Bin

set /p baseLine=Please enter the name of the $/LonMaker build tree (e.g. Dev, v3.2x, etc):

Set SSLMWVerRoot=%SSLMWRoot%\%baseLine%
set LMWBldVerRoot=%LMWBldDir%\%baseLine%
set logdir=%LMWBldDir%
Set P4LONMAKERIMPORTBRANCH=Import_LonMaker_%baseLine%
Set P4LonMakerExportBranch=Export_LonMaker_%baseLine%
rem /DP Set gitUsername="Kevin Patel"
rem /DP Set gitEmailId="kevin.patel@slscorp.com"


:startargs
if "%1" == "" goto endargs
if not "%1" == "-noclean" goto arg2
set noclean=noclean
set p4SyncSwitch=
goto nextarg
:arg2
if not "%1" == "-noupdate" goto arg3
set upd=noupdate
goto nextarg
:arg3

:nextarg
shift

goto startargs
:endargs

Rem /DP Git login 

echo Deleting old log files from %logdir%
if exist %logdir%\*.log del /F /Q %logdir%\*.log

Rem /DP Git pull or simmilar to p4sync

echo Perforce trees are: 
echo .  LM Source : %SSLMWVerRoot%
echo .  LM root   : %LMWBldVerRoot%

echo ** Please verify the above. **
Rem pause

set msdrive=c:

cd %LMWBldDir%

if "%upd%" == "noupdate" goto doneupdt

if "%noclean%" == "noclean" goto skipforce
echo Cleaning all the local source code
echo .

:skipforce

:ImportFiles
Rem Import files 
Echo .
Echo Importing files...
echo Call %toolsBin%\GIT_Import_Export.bat %LMWBldVerRoot%\BuildViews\%P4LONMAKERIMPORTBRANCH%.txt "LonMaker Build imports"
Call %toolsBin%\GIT_Import_Export.bat %LMWBldVerRoot%\BuildViews\%P4LONMAKERIMPORTBRANCH%.txt "LonMaker Build imports"
If ErrorLevel 1 Goto end
:SkipImport
 
echo =============================================================
echo %date% %time%: Updating LonMaker source code (%SSLMWVerRoot%) 
echo %date% %time%: Updating LonMaker source code (%SSLMWVerRoot%) >>%logdir%\build.log
echo =============================================================

:doneupdt

rem
echo Begin LmBuildMain (%LMWBldVerRoot%/BuildScripts/LmBuildMain.bat)
rem
rem
call %LMWBldVerRoot%/BuildScripts/BuildLmMain.bat %argList%

:end
cd %LMWBldDir%


