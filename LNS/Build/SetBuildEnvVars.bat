@Echo Off

Rem     $Header$
Rem
Rem     (c) Copyright 2004-2012, Echelon Corporation.  All rights reserved.
Rem
Rem     SetBuildEnvVars.bat - This file sets all of the environment
Rem     variables used by the various OpenLNS build batch files.  We set
Rem     them all in one place to make it easy to see what environment
Rem     variables are available to build script developers.  This
Rem     file also calls a build-PC-specific batch file named
Rem     SetBuildEnvVars_%PCName%.bat, which sets all of the variables
Rem     that are variable from one build PC to another.
Rem

Rem     Set tracing variables
Rem
Rem Set NUL=CON
Set NUL=NUL

Rem     Set P4 variables used by the p4 client
Set P4USER=p4build
Set P4PORT=PF1:1666

Rem     Set P4 variables used by build script
Set P4CODELINE=Dev
Set P4DEPOTROOT=//depot
Set P4LNSROOT=%P4DEPOTROOT%/Software/NetTools/LNS/%P4CODELINE%
Set P4VNISTACKROOT=%P4LNSROOT%/VniStack
Set P4MTSROOT=%P4DEPOTROOT%/Software/Components/Echelon/LnsMts


Rem     Set names of various P4 branches and workspaces used by the build
Set P4LNSTOOLS=Build_LNS_Tools
Set P4LNSBUILD=Build_LNS_%P4CODELINE%
Set P4LNSEXPORTBRANCH=Export_LNS_%P4CODELINE%
Set P4LNSEXPORTWORKSPACE=Export_LNS_%P4CODELINE%_W


Rem This line will be changed to a number by the p4changelist.bat batch file
Set P4CHANGELIST=Default  

Rem     Set variables containing common source control operations/options.
Rem     All default options are overridable by adding to call site.
Set Get=p4 sync
Set SSGet=SS Get -I-N "-O&-" -R
Set CheckOut=p4 edit
Set CheckIn=p4 submit
Set Revert=p4 revert
Set ChangeDelete=p4 change -d

Rem
Rem     Build configuration
Rem
If "%LnsBuildDebugMode%" == "TRUE" (
    Set CfgType=Debug
) Else (
    Set CfgType=Release
)
Set Cfg=Win32 %CfgType%
Set VSCfg="%CfgType%|Win32"
Set VSCfgMixed="%CfgType%|Mixed Platforms"
Set BldCfg=_Build - %Cfg%
Set BldVniCfg=VniClient - %Cfg%

Rem     Set the mail configuration variables, unless already set for a compound build.
If Not Defined CompoundBuild Call SetMailConfig


Rem     Normally the POET Schema is built each time.  To keep a specific version 
Rem     uncomment the next two lines, and specify the poet DB version (PoeDbDictVer).  
Rem     This assumes that the backup database is stored under OBJ\PoeDb\PoeDbDictVer
Rem Set SkipPoetBuild=True
Rem Set PoeDbDictVer=V3.20

Rem     The final set is for Build-PC-specific paths.  The build PC name
Rem         was passed in as the one argument to this batch file.

Call SetBuildEnvVars_%1
