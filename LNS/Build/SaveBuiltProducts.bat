@Echo Off

Rem     $Header$
Rem
Rem     (c) Copyright 2004-2012, Echelon Corporation.  All rights reserved.
Rem
Rem     SaveBuiltProducts.bat - Save internal files from OpenLNS builds that 
Rem		are needed for future debugging purposes.

if "%LnsBuildDebugMode%" == "FALSE" Set Root=\\virgil\Shared SW\LNS Installations\Internal\System Test
if "%LnsBuildDebugMode%" == "TRUE" Set Root=\\virgil\Shared SW\LNS Installations\Internal\Development
If Not Exist "%Root%" MkDir "%Root%"

if "%LnsBuildDebugMode%" == "FALSE" Set BuiltTgtPath=%Root%\%VerMajor%.%VerMinor%.%VerBuild% (UNTESTED)
if "%LnsBuildDebugMode%" == "TRUE"  Set BuiltTgtPath=%Root%\%VerMajor%.%VerMinor%.%VerBuild%DEBUG

Rem SDK target path
Set LnsSdkTgtPath=%BuiltTgtPath%\LNS SDK

Rem OpenLNS Server target path
Set LnsServerTgtPath=%BuiltTgtPath%\LNS Server

Rem ****************************************
Rem  wipe out previous folder and contents
Rem ****************************************
If Exist "%BuiltTgtPath%" rmdir /Y "%BuiltTgtPath%"
If Exist "%BuiltTgtPath%" Goto :DirBusy
MkDir "%BuiltTgtPath%"

Rem Prepare folders for OpenLNS Server
Mkdir "%LnsServerTgtPath%"
Mkdir "%LnsServerTgtPath%\Standalone"
Mkdir "%LnsServerTgtPath%\Embedded"
Mkdir "%LnsServerTgtPath%\CD Image"

Rem Prepare folders for SDK
Mkdir "%LnsSdkTgtPath%"
Mkdir "%LnsSdkTgtPath%\Standalone"
Mkdir "%LnsSdkTgtPath%\Embedded"

Echo START: Copying internal use OpenLNS files to %Dir%...

Rem ***************************************************************************
Rem                     O p e n L N S    Server
Rem ***************************************************************************
Rem Move the OpenLNS Server
@echo --------------------------------------
@echo      **** Move OpenLNS Server ****
@echo --------------------------------------
copy "%IsProjPath%\LNS_Server\Releases\Release-EXE\DiskImages\DISK1\*.exe"  "%LnsServerTgtPath%\Standalone"
copy "%IsProjPath%\LNS_Server\Data\ObjectServer\Readme.htm"		    "%LnsServerTgtPath%\Standalone"
copy "%IsProjPath%\LNS_Server\Releases\Release-MSI\DiskImages\DISK1\*.msi"  "%LnsServerTgtPath%\Embedded"
copy "%BldPath%\LCA\bin\_SetupOpenLNS.dll"									"%LnsServerTgtPath%\Embedded"
copy "%IsProjPath%\LNS_Server\Releases\Release-EXE\DiskImages\DISK1\*.exe"  "%LnsServerTgtPath%\CD Image"
copy "%BldPath%\Install\LNS_Server\License.rtf"                             "%LnsServerTgtPath%\CD Image"
copy "%BldPath%\Install\LNS_Server\autorun.inf"                             "%LnsServerTgtPath%\CD Image"
copy "%BldPath%\Graphics\OpenLNS.ico"					    "%LnsServerTgtPath%\CD Image"
copy "%IsProjPath%\LNS_Server\Data\ObjectServer\Readme.htm"		    "%LnsServerTgtPath%\CD Image"
attrib -r "%LnsServerTgtPath%\CD Image\*.*

Rem ***************************************************************************
Rem                     O p e n L N S    S D K
Rem ***************************************************************************
Rem Move the OpenLNS SDK
@echo --------------------------------------
@echo        **** Move OpenLNS SDK ****
@echo --------------------------------------
Rem copy "%IsProjPath%\LNS_SDK\Releases\Release-EXE\DiskImages\DISK1\*.exe"	"%LnsSdkTgtPath%\Standalone"
Rem copy "%IsProjPath%\LNS_SDK\Releases\Release-MSI\DiskImages\DISK1\*.msi"	"%LnsSdkTgtPath%\Embedded"


Goto :Exit
:DirBusy
Echo %0: failed to delete folder - perhaps it's in use
Goto :Exit

:Exit
Rem     That's all Folks!
EndLocal
