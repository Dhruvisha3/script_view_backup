Rem $Header$
Rem
Rem SetBuildEnvVars_%PCName%.bat - This file sets all of the variables
Rem	that are variable from one build PC to another.

Rem  Set locations 
Set BldDrive=C:
Set IsToolsDrive=C:
Set BldToolsDrive=C:
Set IsBldDrive=C:
Set CopyRootDrive=%IsBldDrive%
Set BldDir=\LNS\Dev
Set BldPath=%BldDrive%%BldDir%
Set BinDir=C:\Bin
Set PerlDir=C:\Perl
Set IsToolsPath=%IsToolsDrive%\Program Files\InstallShield\2010 StandaloneBuild\System
Set BldToolsPath=%BldPath%\Build
Set IsMergeModulePath=%BldPath%\import\MergeModules
Set IsProjPath=%BldDrive%%BldDir%\Install
Set ThirdPartyDir=%BldDrive%\Components\Others
Set LNISelfInstDir=%BldToolsDrive%\LNI\SelfInstallers
Set DotNETPath=C:\WINDOWS\Microsoft.NET\Framework\v3.5
Set COMMONINCLUDE=..\Import

Rem     Build directories
Set DevEnvDir=C:\Program Files\Microsoft Visual Studio 9.0\VC
Set STLportDir=%ThirdPartyDir%\STLport\V5.2
Set STLportIncDir=%STLportDir%\STLport
Set STLportLibDir=%STLportDir%\Lib\vc9
Set DevEnvToolsDir=C:\Program files\Microsoft Visual Studio 9.0\Common7\Tools
Set DevEnvBinDir=C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE;%DevEnvDir%\bin;%DevEnvToolsDir%
Set PlatformSDKDir=C:\Program Files\Microsoft SDKs\Windows\v6.0A

Rem     Redistributable directories
Set FastObjectsDir=C:\Components\Others\FastObjects\V11.0
