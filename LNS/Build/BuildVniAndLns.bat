@Echo off

Rem     $Header$
Rem
Rem     (c) Copyright 2004-2012, Echelon Corporation.  All rights reserved.

If "%1x" == "x" Goto syntax_check
Set /A DebugBuildNo=%1
Set /A ReleaseBuildNo=%DebugBuildNo% + 1

Echo Build start time:
Date /T
Time /T
Echo.

Rem     Normally, ERRORLEVEL is set to the exit code
Rem     of the last command in a command pipeline, which
Rem     is not what we want below.  To work around this,
Rem     'SaveErr' saves the ERRORLEVEL of the specified
Rem     command, and 'RestErr' restores this last saved
Rem     ERRORLEVEL.

Echo +++++++++++++++++++++++++++
Echo +++ VNI Build
Echo +++++++++++++++++++++++++++
PushD \VNI\Dev
    Call SaveErr Build\BuildVniDebugAndRelease %1 2>&1 | Tee VniBuild%DebugBuildNo%And%ReleaseBuildNo%.log & RestErr
PopD
If ErrorLevel 1 Goto done_error
Echo.

Echo +++++++++++++++++++++++++++
Echo +++ OpenLNS Build
Echo +++++++++++++++++++++++++++
PushD \LNS\Dev
    Call SaveErr Build\BuildLnsDebugAndRelease %1 2>&1 | Tee LnsBuild%DebugBuildNo%And%ReleaseBuildNo%.log & RestErr
PopD
If ErrorLevel 1 Goto done_error
Echo.


:successExit
    Echo ---------------------------
    Echo VNI ^& OpenLNS Build Success
    Echo ---------------------------
    Goto done

:syntax_check
    Echo ****************************
    Echo BuildVniAndLns syntax:
    Echo	BuildVniAndLns <BuildNum>
    Echo ****************************
    Goto done

:done_error
    Echo ****************************
    Echo Error building VNI ^& OpenLNS
    Echo ****************************

:done
    Echo.
    Echo Build End time:
    Date /t
    Time /t
