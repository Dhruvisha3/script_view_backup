' CreateDataTagFile.vbs - Create the Data.tag file that is placed in the LNS Server
'			  and Remote redistributables to tell the LNS Redistributable
'			  Maker the version number of the runtimes.  This allows the 
'			  build script to update the version number with every build.
' arg1 = Path to Data.tag file
' arg2 = Application name (LNS Server or LNS Remote)
' arg3 = Version Number (example, 3.20.50, for LNS major release 3, minor release 20, build 50)
'
Option Explicit

Dim argNum, argCount:argCount = Wscript.Arguments.Count
If (argCount < 3) Then
	Wscript.Echo "CreateDataTagFile called without the required three arguments"
	Wscript.Quit 1
End If

Dim DataTagPath : DataTagPath = Wscript.Arguments.Item(0)
Dim AppName     : AppName     = Wscript.Arguments.Item(1)
Dim VersionNum  : VersionNum  = Wscript.Arguments.Item(2)
	
Wscript.Echo DataTagPath
Wscript.Echo AppName
Wscript.Echo VersionNum

' Get a file system access object
Dim objFSO : Set objFSO = Nothing
Set objFSO = Wscript.CreateObject("Scripting.FileSystemObject") : CheckError
Dim objFile

' Open the LNS Server data.tag file
Set objFile = objFSO.CreateTextFile(DataTagPath)

' Write the file contents, including the version information
objFile.WriteLine ("[TagInfo]")
objFile.WriteLine ("Company=Echelon Corporation")
objFile.Write ("Application=")
objFile.WriteLine (AppName)
objFile.Write ("Version=")
objFile.WriteLine (VersionNum)
objFile.WriteLine ("Category=Internet Tool")
objFile.WriteLine ("Misc=")
objFile.Close

' Done...
Wscript.Quit 0


'
'  Subroutines
'
Sub CheckError
	Dim message, errRec
	If Err = 0 Then Exit Sub
	message = Err.Source & " " & Hex(Err) & ": " & Err.Description
	If Not objFSO Is Nothing Then
		Set errRec = objFSO.LastErrorRecord
		If Not errRec Is Nothing Then message = message & vbLf & errRec.FormatText
	End If
	Fail message
End Sub

Sub Fail(message)
	Wscript.Echo message
	Wscript.Quit 2
End Sub
