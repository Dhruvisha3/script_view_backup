rem
rem  SetBuildEnvVars_%PCName%.bat - This file sets all of the variables
rem	that are variable from one build PC to another.
rem

rem  Set locations 
Set BldDrive=D:
Set IsToolsDrive=D:
Set BldToolsDrive=D:
Set IsBldDrive=C:
Set CopyRootDrive=%IsBldDrive%
Set BldDir=\LNS\dev
Set BldPath=%BldDrive%%BldDir%
Set BinDir=C:\Bin
Set PerlDir=C:\Perl
Set IsToolsPath=%IsToolsDrive%\Program Files\InstallShield\StandaloneBuild9SP1
Set BldToolsPath=%BldPath%\Build
Set IsProjDir=%BldDir%\InstallShield
Set IsProjPath=%IsBldDrive%%IsProjDir%
Set IsMergeModulePath=%ISProjPath%\MergeModules
Set CopyRootDir=%IsProjPath%\InstallInput
Set ThirdPartyDir=%BldDrive%\Redist
Set LNISelfInstDir=%BldDrive%\LNI\Dev\SelfInstallers

rem     Build directories
Set MsDevDir=C:\Program Files\Microsoft Visual Studio\VC98
Set STLportDir=%MsDevDir%\Include\STLport
Set PlatformSDKDir=D:\Program Files\Microsoft SDK
Set FastObjectsDir=D:\Program Files\FastObjects_t7_9.5

