@Echo Off

Rem     $Header$
Rem
Rem     (c) Copyright 2004-2014, Echelon Corporation.  All rights reserved.

Rem     Automatically determine the local absolute location of
Rem     the build tools folder as the folder containing this script.
Set BldToolsPath=%~dp0

Rem     Automatically determine the local absolute location of
Rem     the OpenLNS start folder as the parent of the build tools folder.
For %%R In (%BldToolsPath%..) Do Set LnsStartFolder=%%~fR

Rem     Add the build folder (location of this script) to the PATH
Rem     so that we can call the build tools from any location.
Set Path=%Path%;%~dp0


Rem     check PATH for required tools
Set TOOLS=p4.exe perl.exe blat.exe timein.bat p4import.bat p4export.bat SetMailConfig.bat BuildLnsC.bat SymStore.exe
For %%e In (%TOOLS%) Do If "%%~$PATH:e" == "" (
    Echo ERROR: cannot find "%%e"
    Echo This build script requires the tools: %TOOLS%
    Exit /B 1
) >&2


Goto :Start

:SetConfiguration
    Set LNSVersion=4.40
    Set P4CODELINE=Dev

    Set P4LNSRoot=//depot/Software/NetTools/LNS/%P4CODELINE%
    Set CompoundBuild=TRUE

    Rem     (Re)set the mail configuration variables.
    Call SetMailConfig
    Set SkipEmail=True
    
    Rem     Override the default estimated build time for this double-build.
    Set BuildTime=120
Goto :EOF


:Start

Call :SetConfiguration


Rem     Extract initial build number from command line, 
Rem     and generate 3-digit Debug & Release build numbers from it, 
Rem     prepending '0's if less than 10, and/or less than 100.
Set /A DebugBuildNo=%1
Set /A ReleaseBuildNo=%DebugBuildNo% + 1
Set _DebugBuildNo=%DebugBuildNo%
Set _ReleaseBuildNo=%ReleaseBuildNo%
If %_DebugBuildNo%   LSS 10 (Set _DebugBuildNo=00%_DebugBuildNo%)     Else If %_DebugBuildNo%   LSS 100 Set _DebugBuildNo=0%_DebugBuildNo%
If %_ReleaseBuildNo% LSS 10 (Set _ReleaseBuildNo=00%_ReleaseBuildNo%) Else If %_ReleaseBuildNo% LSS 100 Set _ReleaseBuildNo=0%_ReleaseBuildNo%


Rem     Extract (optional) machine name (configuration file suffix) 
Rem     from command line, defaulting to this machine's name.
Set MachineName=%COMPUTERNAME%
If Not "%~2" == "" Set MachineName=%~2


Echo You are about to build (on %MachineName%):
Echo.    OpenLNS Debug   build %_DebugBuildNo%, followed by...
Echo.    OpenLNS Release build %_ReleaseBuildNo%.
Echo.
Echo ========================================
Echo Press Ctrl-C now if you want to abort...
Echo ========================================
Echo.

Pause && Echo.


Rem	    Compute estimated build completion time.
Call TimeIn %BuildTime% DoneTime


Rem	    Send build in progress notice via email.
If Not Defined SkipEmail (
    Echo Sending build notice ^(estimated completion at %DoneTime%^)...
    Blat -q -install %MailServer% %MailFrom% 3
    Blat -  -subject "OpenLNS %LNSVersion% Build In Progress" ^
            -to %MailTo_Start% -q ^
            -body "OpenLNS %LNSVersion%.%_DebugBuildNo% (Debug) & %LNSVersion%.%_ReleaseBuildNo% (Release) builds are now in progress.  Please refrain from submitting changes to %P4LNSRoot%/... until after %DoneTime% PT." ^
            -debug -log blat.log
    If ErrorLevel 1 Echo NOTICE: Email notice delivery failed!
)


Echo Starting DEBUG build...
PushD "%LnsStartFolder%"
    Set CurrentBuild=OpenLNS %LNSVersion%.%_DebugBuildNo% (Debug)
    Call BuildLnsC %DebugBuildNo% "%MachineName%" DEBUG
PopD
If ErrorLevel 1 Goto done_error
Echo.

Echo Starting RELEASE build...
PushD "%LnsStartFolder%"
    Set CurrentBuild=OpenLNS %LNSVersion%.%_ReleaseBuildNo% (Release)
    Call BuildLnsC %ReleaseBuildNo% "%MachineName%"
PopD
If ErrorLevel 1 Goto done_error
Echo.



:done_noerror
    Rem     Set configuration again in case it was changed by one of the builds.
    Call :SetConfiguration
    
    Rem     Fetch the time now.
    Call TimeIn 0 DoneTime
    
    Rem	    Send build success notice via email.
    If Not Defined SkipEmail (
        Echo Sending build completion notice...
        Blat - -subject "OpenLNS %LNSVersion% Build Complete"       ^
                -to %MailTo_Success% -q                             ^
                -body "OpenLNS %LNSVersion%.%_DebugBuildNo% (Debug) & %LNSVersion%.%_ReleaseBuildNo% (Release) builds completed successfully at %DoneTime% and are now ready for Smoke Test." ^
                -debug -log blat.log
        If ErrorLevel 1 Echo NOTICE: Email notice delivery failed!
    )
    
    Exit /B 0


:done_error
    Rem     Set configuration again in case it was changed by one of the builds.
    Call :SetConfiguration
    
    Rem     Fetch the time now.
    Call TimeIn 0 DoneTime
    
    Rem	    Send build failure notice via email.
    If Not Defined SkipEmail (
        Echo Sending build failure notice...
        Blat - -subject "OpenLNS %LNSVersion% Build Failed"         ^
               -to %MailTo_Failure% -priority 1 -q                  ^
               -body "%CurrentBuild% build failed at %DoneTime%!||See error log at: \\%MachineName%\%BldDir%\Build\LnsBld.log"   ^
               -debug -log blat.log
        If ErrorLevel 1 Echo NOTICE: Email notice delivery failed!
    )

    Exit /B 1
