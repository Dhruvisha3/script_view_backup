@echo off

Rem     $Header$
Rem
Rem     (c) Copyright 2004-2012, Echelon Corporation.  All rights reserved.

@echo ..
@echo ..  !!   Build the OpenLNS Installers, including the OpenLNS Server,    !!
@echo ..  !!   OpenLNS SDK, and the OpenLNS Server Edition.                   !!
@echo ..

Set _PATH=%PATH%
Set PATH=%IsBuildPath%
@echo PATH="%PATH%"

rem  Set the Product Version string for use throughout
Set ProductVersion=%vermajor%.%verminor%.%verbuild%

@echo ..
@echo ..  The InstallShield Tools directory and project directory must
@echo ..  have been previously set up.  The Tools directory is where the
@echo ..  InstallShield 2013 standalone build tools have been
@echo ..  installed.  The Project Directory should have been previously
@echo ..  been set up by the PrepareLnsFiles.bat file in the build script,
@echo ..  which will get all of the files for the installation, time and
@echo ..  date stamp them, and put them in the appropriate directory tree
@echo ..  as components for building the various OpenLNS-based products.  It
@echo ..  should also get the latest InstallShield project files from
@echo ..  source control.
@echo ..

CD %IsProjPath%
if ErrorLevel 1 Goto done_error

@echo -------------------------------------
@echo  ***** Build the Merge Modules *****
@echo -------------------------------------
For %%d In (BinaryConverters LnsPrimaryInteropAssembly) Do (
	Call :BuildMergeModule %%d
	If ErrorLevel 1 Goto done_error
)

@echo ------------------------------------------
@echo  ***** Build the OpenLNS Server Install *****
@echo ------------------------------------------
For %%d In (LNS_Server LNS_Server_Silent) Do (
	Call :BuildInstallComponent %%d
	If ErrorLevel 1 Goto done_error
)

goto done_noerror

:done_error
Echo ...
Echo ... OpenLNS generated a real error.  Look at Installer build .log files for cause
Echo ...
Echo .
set junkVal=1

Goto End


:done_noerror

CD %BldPath%

Goto End


Rem     --- BEGIN SUBROUTINES --------------------------------------------------------

Rem	Subroutine:  BuildInstallComponent
Rem     Subroutine to build the InstallShield component
Rem      	arg1 - The project name
:BuildInstallComponent
@echo -------------------------------------------
@echo   Build the %1 Installer
@echo -------------------------------------------

cd %1
erase %1.ism.doublecheck
copy %1.ism %1.ism.backup
attrib -r  %1.ism

Rem Set new Product and Package codes that will be shared by both MSI and EXE
cscript //nologo %BinDir%\SetNewMsiProductCodeIs2013.vbs %1.ism
cscript //nologo %BinDir%\SetNewMsiPackageCodeIs2013.vbs %1.ism

Rem     NOTE: This should no longer be necessary if using
Rem           the InstallShield marker "***ALL_VERSIONS***".
Rem Set the version number in the Upgrade Table, to identify higher versions
Rem Call %BinDir%\SetInstallShieldUpgradeVer "%IsProjPath%\%1\%1.ism" %ProductVersion%
 
If ErrorLevel 1 Goto done_error

rem Build the EXE installer via the InstallShield standalone tools
ISCmdBld.exe -p %1.ism -x -o %IsMergeModulePath% -y "%ProductVersion%" -a "Releases" -r "Release-EXE"
If ErrorLevel 1 Goto done_error

rem Build the MSI installer via the InstallShield standalone tools
ISCmdBld.exe -p %1.ism -x -o %IsMergeModulePath% -y "%ProductVersion%" -a "Releases" -r "Release-MSI"
If ErrorLevel 1 Goto done_error

Rem Restore the InstallShield project file to its former values
copy %1.ism %1.ism.doublecheck
erase %1.ism
rename %1.ism.backup %1.ism
attrib +r  %1.ism

CD %IsProjPath%

Goto :EOF

Rem	Subroutine:  BuildMergeModule
Rem     Subroutine to build the InstallShield component
Rem      	arg1 - The project name
:BuildMergeModule
@echo -------------------------------------------
@echo   Build the %1 Merge Module
@echo -------------------------------------------

cd %1

rem Build the Merge Module via the InstallShield standalone tools
ISCmdBld.exe -p %1.ism -x -o %IsMergeModulePath% -y "%ProductVersion%"
If ErrorLevel 1 Goto done_error

CD %IsProjPath%

Goto :EOF


Rem     --- END SUBROUTINES ----------------------------------------------------------


:End

rem Set path back to initial value
Set PATH=%_PATH%

@echo ..  End of InstallShield 2013 project builds


