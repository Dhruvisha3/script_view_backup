Rem     $Header$
Rem
Rem     (c) Copyright 2004-2012, Echelon Corporation.  All rights reserved.

Rem SetBuildEnvVars_%PCName%.bat - This file sets all of the variables
Rem	that are variable from one build PC to another.

Rem  Set locations 
Set BldDrive=C:
Set IsToolsDrive=C:
Set BldToolsDrive=C:
Set IsBldDrive=C:
Set CopyRootDrive=%IsBldDrive%
Set BldDir=\LNS\Dev
Set BldPath=%BldDrive%%BldDir%
Set BinDir=C:\Bin
Set PerlDir=C:\Perl
Set IsToolsPath=%IsToolsDrive%\Program Files\InstallShield\2010 StandaloneBuild\System
Set BldToolsPath=%BldPath%\Build
Set IsMergeModulePath=%BldPath%\import\MergeModules
Set IsProjPath=%BldDrive%%BldDir%\Install
Set ThirdPartyDir=%BldDrive%\Components\Others
Set LNISelfInstDir=%BldToolsDrive%\LNI\SelfInstallers
Set DotNETPath=C:\WINDOWS\Microsoft.NET\Framework\v3.5
Set COMMONINCLUDE=..\Import

Rem     Build directories
Set DevEnvDir=C:\Program Files\Microsoft Visual Studio 9.0\VC
Set DevEnvToolsDir=C:\Program files\Microsoft Visual Studio 9.0\Common7\Tools
Set DevEnvBinDir=C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE;%DevEnvDir%\bin;%DevEnvToolsDir%
Set PlatformSDKDir=C:\Program Files\Microsoft SDKs\Windows\v6.0A

Set STLportDir=%ThirdPartyDir%\STLport\V5.2
Set STLportIncDir=%STLportDir%\STLport
Set STLportLibDir=%STLportDir%\Lib\vc9
Set STLportBinDir=%STLportDir%\Bin\vc9

Set BoostDir=%ThirdPartyDir%\Boost\V1.39
Set BoostIncDir=%BoostDir%
Rem Set BoostLibDir=%BoostDir%\{appropriate LIB path here}
Rem Set BoostBinDir=%BoostDir%\{appropriate BIN path here}

Rem     Redistributable directories
Set FastObjectsDir=C:\Program Files\Versant\FastObjects_t7_11.0

Rem	Build PATHs - Make this independent of the Build PC environment variables, this must be tightly controlled
Set ToolsPaths=C:\bin;C:\Bin\bin
Set WinPaths=C:\Windows\System32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0
Set PerforcePath=C:\Program Files\Perforce
Set WinSdkPaths=C:\Program Files\Debugging Tools For Windows (x86);%PlatformSDKDir%
Set LonWorksPath=C:\Program Files\LonWorks\bin
Set EfcPath=C:\Export\EFC\Bin\vc9
Set BuildPath=%ToolsPaths%;%WinPaths%;%PerforcePath%;%WinSdkPaths%;%LonWorksPath%;%EfcPath%;%STLportBinDir%
Set IsBuildPath=%BuildPath%;%IsToolsPath%

