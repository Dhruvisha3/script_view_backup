@Echo Off

Rem     $Header$
Rem
Rem     (c) Copyright 2004-2012, Echelon Corporation.  All rights reserved.
Rem
Rem     Mail Configuration
Rem
Rem     NOTE - MailFrom (BuildMaster) must specify only one email address;
Rem            others may specify multiple comma-separated addresses.
Rem
Rem     NOTE - Our mail server does not support relaying of messages
Rem            to external email addresses, so only include
Rem            @echelon.com email addresses here.
Rem

Set BuildMaster=GroupSoftwareTools@echelon.com
Set SmokeTester=mmilsner@echelon.com
REM Set FollowOnBuilder=ddemoney@echelon.com
Set NotifyGroup=GroupSoftwareTools@echelon.com
Set MailServer=smtp.echelon.com
Set MailFrom="OpenLNS Build <%BuildMaster%>"
Set MailTo_Start=%NotifyGroup%
REM Set MailTo_Success=%NotifyGroup%,%SmokeTester%,%FollowOnBuilder%
Set MailTo_Success=%NotifyGroup%,%SmokeTester%
Set MailTo_Failure=%NotifyGroup%

Rem     Estimated build time (in minutes).
Set BuildTime=80
