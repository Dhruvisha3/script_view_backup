@Echo Off

Rem     $Header$
Rem
Rem     (c) Copyright 2004-2012, Echelon Corporation.  All rights reserved.

Rem     Automatically determine the local absolute location of
Rem     the build tools folder as the folder containing this script.
Set BldToolsPath=%~dp0

Rem     Automatically determine the local absolute location of
Rem     the OpenLNS start folder as the parent of the build tools folder.
For %%R In (%BldToolsPath%..) Do Set LnsStartFolder=%%~fR

Rem     Add the build folder (location of this script) to the PATH
Rem     so that we can call the build tools from any location.
Set Path=%Path%;%BldToolsPath%


Rem     Only uncomment the next statement when making build SCRIPT
Rem     modifications and testing them.  This will prevent the build
Rem     from sending out e-mail progress updates, but you must be
Rem     sure to prevent loss of important build PC data!!!!!!
Rem Set LnsBuildScriptDebug=True


Echo.
Echo.
Echo   ******************************************************
Echo.
Echo     OpenLNS Build Script
Echo.
Echo     This will perform a production build of OpenLNS, and
Echo     will get files from source control, and will
Echo     integrate and label files as well.
Echo.
Echo   ******************************************************
Echo.


Rem     Set an environment variable to tell whether we are building
Rem     in debug mode or release mode.  We will build in debug mode
Rem     if a fourth parameter is passed in, no matter what its value.
If "%3x" == "x" Goto set_release
    Echo   ******************************************************
    Echo   *************** DEBUG MODE BUILD *********************
    Echo   ******************************************************
    Set LnsBuildDebugMode=TRUE
    Set DebugBuild=_Debug
    echo Debug build
    Goto done_setting_build_mode
    
:set_release
    Echo   ******************************************************
    Echo   ************** RELEASE MODE BUILD ********************
    Echo   ******************************************************
    Set LnsBuildDebugMode=FALSE
    set DebugBuild=
    Goto done_setting_build_mode
    
:done_setting_build_mode
Echo.

If Not Defined CompoundBuild Pause


Rem     Extract (optional) machine name (configuration file suffix) 
Rem     from command line, defaulting to this machine's name.
If Not Defined MachineName (
    Set MachineName=%COMPUTERNAME%
    If Not "%~3" == "" Set MachineName=%~3
)

Rem     Set all of the Build Environment variables that are used in
Rem     this batch files and all of the batch files that are called
Rem     by this batch file.
Call SetBuildEnvVars "%MachineName%"

Rem     add OBJ\Bin to path to allow proper registration
Set Path=%Path%;%BldPath%\OBJ\Bin


Rem     disable use of Perforce configuration files
Rem     so that environment variables can be used
Set P4CONFIG=NUL


Rem     make sure we are logged into Git as 'p4build'
Rem /SS git login check

Rem     sync latest utility files from source control
Rem /SS git checkout/clone tool\bin repo
Echo Updating utility files (Workspace: %git_lnsdev%)...
Rem /SS git pull above repo
Echo.


Rem     switch to main client workspace
Rem /SS back to main lns repo


Rem     sync all source files at start
Rem
Echo Updating source files (Workspace: %git_lnsdev%) to latest version...
Rem /SS git pull main lns repo
Echo.


Rem     fetch current (previous) version number header file
Echo Checking version number header...
Call GetLnsVer -e -f Import\version.h
Echo   Version.h is currently at V%VerMajor%.%VerMinor%.%VerBuild%

Rem     The new build number is on the command line.  Do not try to perform operations on the
Rem	VerBuild number from the include file, as it probably has leading zeroes, will be
Rem	mistaken for an Octal constant because of that, and could throw an out-of-range error 
Rem	in the process.
If "%1x" == "x" Goto bad_syntax
Set /a newBuildNo=%1
Set /a oldbuildNo=000

Rem     prepend '0's if less than 10, and/or less than 100
If %newBuildNo% LSS 10 (Set newBuildNo=00%newBuildNo%) Else If %newBuildNo% LSS 100 Set newBuildNo=0%newBuildNo%

Rem Echo   Old build number was     %oldBuildNo%
Echo   New build number will be %newBuildNo%
Echo.

Set ProductVersion=%VerMajor%.%VerMinor%.%newBuildNo%

Rem     update version number header file, if neccessary

:set_version_info

If "%newBuildNo%" == "%VerBuild%" Goto have_uptoDate

Rem     In future date, set all variables, not just build number:
Rem
Rem     Call setlnsver -f version.h %VerMajor% %VerMinor% %newBuildNo%
:must_edit_version
    Echo Updating OpenLNS' version number header (version.h)...
    
	Echo Updating C++ project version numbers...
    Call SetLnsVer  -f "Import\version.h" %newBuildNo%                   || Goto done_error

    Echo Updating C# project version numbers...
	Call SetAssemblyVer "Europe\LnsDbWizard\LNS Database Recovery Wizard\Properties\AssemblyInfo.cs" %newBuildNo% 

    Set VerBuild=%newBuildNo%
    Echo.
    Goto skip_have_uptoDate

:have_uptoDate
    Echo Version.h is already up to date.
    Echo.
	
:skip_have_uptoDate

Rem     submit the open changelist
Echo Submitting version file changes...
If Defined LnsBuildScriptDebug (
	Echo Debugging script: skipping version file checkin
	Goto version_checkin_done
)

	git add "Import\version.h"
	git add "Europe\LnsDbWizard\LNS Database Recovery Wizard\Properties\AssemblyInfo.cs"
	git commit -m "Version updates for V%ProductVersion%."
	git push origin %git_lnsdev_branch%      
    If ErrorLevel 1 (
        Rem if this fails, something else may have gone wrong, so abort
        If ErrorLevel 1 goto done_error
    )
    Set LabeledChangesDone=1
    Echo.

:version_checkin_done


Rem     switch to main build workspace
Rem /SS back to main lns repo


Rem     Import global external (upstream) modules (e.g. LnsMts) 
Rem     (libraries and header files).
:doImports
    Echo Importing global upstream projects...
	Echo Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Import_LNS_%P4CODELINE%.txt "Imported files for project located at '%git_lnsdev%' for the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Import_LNS_%P4CODELINE%"
	Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Import_LNS_%P4CODELINE%.txt "Imported files for project located at '%git_lnsdev%' for the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Import_LNS_%P4CODELINE%"
    Echo.
    

Rem     sync all source files to build changelist
Rem
Echo Syncing source files to changelist %LabeledChanges% (Workspace: %git_lnsdev%)...
Rem /SS git pull main lns repo
Echo.


:do_work

Rem     Now that we know the correct version numbers, update target
Rem     directory and remove temporary copy of Version.h.
Rem Set TargetDir=%TargetDir%\LNS_%VerMajor%%VerMinor%.%newBuildNo%
Rem Set TargetRedistDir=%TargetRedistDir%\LNS_%VerMajor%%VerMinor%.%newBuildNo%

If Defined SkipPoetBuild Goto DictionaryOk
    Rem     Before we do too much work, check that we have the required
    Rem     prior-version dictionary files.
    If Exist OBJ\poedb\%VerMajor%_%VerMinor%_%oldBuildNo%\dictionary\_objects.* Goto DictionaryOk
    
    Echo *** Getting dictionary from source control
    If Not Exist OBJ\POEDB Mkdir OBJ\POEDB
    PushD OBJ\POEDB
        Rem Call %Get% %P4Root%/obj/poedb/_objects.*@%LabeledChanges%
        If Not Exist "%VerMajor%_%VerMinor%_%oldBuildNo%" MkDir "%VerMajor%_%VerMinor%_%oldBuildNo%"
        Cd "%VerMajor%_%VerMinor%_%oldBuildNo%"
        If Not Exist dictionary Mkdir dictionary
    PopD
    Copy OBJ\POEDB\_objects.* OBJ\POEDB\%VerMajor%_%VerMinor%_%oldBuildNo%\dictionary

    If Exist OBJ\POEDB\%VerMajor%_%VerMinor%_%oldBuildNo%\dictionary\_objects.* Goto DictionaryOk
    Echo *** Error: cannot find old (OBJ\POEDB\%VerMajor%_%VerMinor%_%oldBuildNo%) POET dictionary files!
    Goto done_error
:DictionaryOk

Rem     if debugging the build script, skip the notification e-mail
If Defined LnsBuildScriptDebug Goto the_Great_Switch

Rem     if build is part of a compound build, skip the notification e-mail, as it may already be sent
If Defined CompoundBuild Goto the_Great_Switch

Rem     compute estimated build completion time
Call TimeIn %BuildTime% DoneTime

If Defined SkipEmail Goto startEmailDone
    Rem	    send build in progress notice via email
    Echo Sending build notice (estimated completion at %DoneTime%)...
    blat -q -install %MailServer% %MailFrom% 3
    blat - -subject "OpenLNS %ProductVersion% Build In Progress" -to "%MailTo_Start%" -q -body "The LNS %ProductVersion% build is now in progress.  Please refrain from submitting changes to %P4LNSROOT%/... until after %DoneTime% PT." -debug -log blat.log
    If ErrorLevel 1 Echo NOTICE: Email notice delivery failed!
:startEmailDone



Rem     ----------------------------------------------------------------------
:the_Great_Switch
    Rem Goto do_nss
    Rem Goto do_formatter
    Rem Goto do_das
    Rem Goto do_obj
    Rem Goto do_eg
    Rem Goto do_lca
    Rem Goto do_europe
    Rem Goto do_lns
    Rem Goto do_save_internal_files
    Rem Goto do_build_installers
    Rem Goto do_get_demoshield_files
    Rem Goto do_publish_build

Rem     ----------------------------------------------------------------------
Rem     NOTE: A pre-built and exported version of LnsMts is now
Rem     imported into OpenLNS instead of being built here as part of LNS.
Rem     ----------------------------------------------------------------------

Rem     ----------------------------------------------------------------------
Rem     NOTE: VNI component has been moved out of the main OpenLNS build and 
Rem     is imported into OpenLNS now.
Rem     ----------------------------------------------------------------------


Rem     ----------------------------------------------------------------------
Rem     NOTE: A pre-built and exported version of OpenLDV is now
Rem     imported into OpenLNS instead of being built here as
Rem     part of OpenLNS (this means that the OpenLDV version
Rem     numbers will no longer track those of OpenLNS).
Rem     ----------------------------------------------------------------------

Rem     ----------------------------------------------------------------------
Rem     LDRF files are now imported into OpenLNS by branchspec Import_Lns_Dev
Rem     ----------------------------------------------------------------------


Rem     ----------------------------------------------------------------------
:do_nss
    CD %BldDir%\NSS|| goto done_error

    Rem     Import (and Sync) upstream projects (e.g. VniStack).
    Echo Importing upstream projects using branchspec Import_LNS_Dev_NSS%DebugBuild% ...
    Echo Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Import_LNS_Dev_NSS%DebugBuild%.txt "Imported files for project located at '%git_lnsdev%\NSS' for the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Import_LNS_Dev_NSS%DebugBuild%"
	Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Import_LNS_Dev_NSS%DebugBuild%.txt "Imported files for project located at '%git_lnsdev%\NSS' for the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Import_LNS_Dev_NSS%DebugBuild%"
    Echo.


    If Exist "nsstemp" RD /s /q "nsstemp"

    Call build\nssbuild2008.bat -s
    If ErrorLevel 1 Goto done_error


    Rem     Export NSS target and source files
    Echo Export NSS source and binaries for use by downstream components.
    Echo Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Export_LNS_Dev_Nss_Source.txt "Exported file from project located at '%git_lnsdev%\NSS' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Export_LNS_Dev_Nss_Source"
	Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Export_LNS_Dev_Nss_Source.txt "Exported file from project located at '%git_lnsdev%\NSS' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Export_LNS_Dev_Nss_Source"    
    Rem Echo.




Rem     ----------------------------------------------------------------------
:do_formatter
    CD %BldDir%\Formatter|| goto done_error
    Echo Performing Formatter build...
    Call :BuildProduct Formatter || Goto done_error
    
    Echo Exporting Formatter source and binary files post-build ...
    Echo Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Export_LNS_Dev_Formatter_Source.txt "Exported file from project located at '%git_lnsdev%\Formatter' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Export_LNS_Dev_Formatter_Source"
	Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Export_LNS_Dev_Formatter_Source.txt "Exported file from project located at '%git_lnsdev%\Formatter' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Export_LNS_Dev_Formatter_Source"
	Echo Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Export_LNS_Dev_Formatter_binaries_LnsBuild.txt "Exported file from project located at '%git_lnsdev%\Formatter' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Export_LNS_Dev_Formatter_binaries_LnsBuild"
	Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Export_LNS_Dev_Formatter_binaries_LnsBuild.txt "Exported file from project located at '%git_lnsdev%\Formatter' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Export_LNS_Dev_Formatter_binaries_LnsBuild"    

Rem     ----------------------------------------------------------------------
:do_das
    CD %BldDir%\DasVni|| goto done_error
    Echo Starting DasVni builds...
    Call :BuildProduct DasVni LnsDs "Debug|Win32" && Copy DasVni.out LnsDsD.out || Goto done_error
    Call :BuildProduct DasVni || Goto done_error

    Echo Exporting DAS source and binary files post-build ...
    Echo Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Export_LNS_Dev_DasVni_source.txt "Exported file from project located at '%git_lnsdev%\DasVni' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Export_LNS_Dev_DasVni_source"
	Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Export_LNS_Dev_DasVni_source.txt "Exported file from project located at '%git_lnsdev%\DasVni' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Export_LNS_Dev_DasVni_source"
	Echo Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Export_LNS_Dev_DasVni_binaries_LnsBuild.txt "Exported file from project located at '%git_lnsdev%\DasVni' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Export_LNS_Dev_DasVni_binaries_LnsBuild"
	Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Export_LNS_Dev_DasVni_binaries_LnsBuild.txt "Exported file from project located at '%git_lnsdev%\DasVni' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Export_LNS_Dev_DasVni_binaries_LnsBuild"    
Rem     ----------------------------------------------------------------------
:do_obj
    CD %BldDir%\Obj|| goto done_error

    Rem     Reset POET schema to specified old version
    Rem If Exist "PoeDB\%VerMajor%_%VerMinor%_%oldBuildNo%\dictionary" XCopy /E /I /R /Y "PoeDB\%VerMajor%_%VerMinor%_%oldBuildNo%\dictionary" PoeDB\dictionary > %NUL%

    Rem     Clean up any prior build targets
    If Exist Bin\*.oca Del Bin\*.oca

    Rem     Optionally build POET Schema
    If Not Defined SkipPoetBuild (
        Echo Building POET Schema...
        
        Attrib -R Lib\PoeDB.lib

        Call :BuildProduct Obj _Build1 && Copy Obj.out Obj_1.out || Goto done_error
     ) Else (
        Echo Copying POET Schema from source control...
        
        If Not Exist "PoeDB\dictionary" MkDir "PoeDB\%dictionary"
        
        XCopy /Y /E /I PoeDB\%PoeDbDictVer%\Dictionary\_objects.*   PoeDB\Dictionary    > %NUL%
        XCopy /Y /E /I PoeDB\%PoeDbDictVer%\BackupDb\objects.*      PoeDB\base          > %NUL%
        XCopy /Y /E /I PoeDB\%PoeDbDictVer%\BackupDb\recovery\*.rcy PoeDB\base\recovery > %NUL%
     )

    Rem     Build the rest of OBJ
    Call :BuildProduct Obj _Build2 && Copy Obj.out Obj_2.out || Goto done_error
    

    Rem     Archive off dictionary and empty database
    If Not Exist "PoeDB\%VerMajor%_%VerMinor%_%newBuildNo%\dictionary" MkDir "PoeDB\%VerMajor%_%VerMinor%_%newBuildNo%\dictionary"
    If Not Exist "PoeDB\%VerMajor%_%VerMinor%_%newBuildNo%\base"       MkDir "PoeDB\%VerMajor%_%VerMinor%_%newBuildNo%\base"
    XCopy /E /I /Y PoeDB\dictionary "PoeDB\%VerMajor%_%VerMinor%_%newBuildNo%\dictionary" > %NUL%
    XCopy /E /I /Y PoeDB\base       "PoeDB\%VerMajor%_%VerMinor%_%newBuildNo%\base"       > %NUL%

    Echo Exporting Obj source and binary files post-build ...
	Echo Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Export_LNS_Dev_Obj_binaries_LnsBuild.txt "Exported file from project located at '%git_lnsdev%\Obj' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Export_LNS_Dev_Obj_binaries_LnsBuild"
	Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Export_LNS_Dev_Obj_binaries_LnsBuild.txt "Exported file from project located at '%git_lnsdev%\Obj' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Export_LNS_Dev_Obj_binaries_LnsBuild"


Rem     ----------------------------------------------------------------------
Rem TODO skip for now
Echo Skipping Examples build...
Echo .
Goto do_lca
:do_eg
    CD %BldDir%\Examples\LcaNetMgmt|| goto done_error
    Call :BuildProduct LcaNetMgmt || Goto done_error

	Rem TODO Obsoleted
    Rem CD %BldDir%\Examples\LcaMonitor|| goto done_error
    Rem Call :BuildProduct LcaMonitor || Goto done_error


Rem     ----------------------------------------------------------------------
:do_lca
    CD %BldDir%\Lca|| goto done_error

    Echo Performing Lca build...
    Call :BuildProduct Lca || Goto done_error

    Rem Echo Exporting Lca source and binary files post-build ...
    Rem Call p4Export -b Export_LNS_Dev_Lca_Source -w Export_LNS_Dev_Lca_binaries -i || Goto done_Error


Rem     ----------------------------------------------------------------------
:do_HelpFiles



Rem     ----------------------------------------------------------------------
:do_europe
    CD %BldDir%\Europe\LnsDbWizard|| goto done_error
    Echo Performing LnsDbWizard build...
    Call :BuildProduct LnsDbWizard _Build %VSCfgMixed% || Goto done_error

    Echo Exporting LnsDbWizard binary files post-build ...
	Echo Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Export_LNS_Dev_LnsDbWizard_Target_LnsBuild.txt "Exported file from project located at '%git_lnsdev%\Europe\LnsDbWizard' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Export_LNS_Dev_LnsDbWizard_Target_LnsBuild"
	Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Export_LNS_Dev_LnsDbWizard_Target_LnsBuild.txt "Exported file from project located at '%git_lnsdev%\Europe\LnsDbWizard' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Export_LNS_Dev_LnsDbWizard_Target_LnsBuild"    

:do_lns

:do_save_internal_files


Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem
Echo ...
Echo ... save lib and pdb's
Echo .
Echo	PATH=%PATH%

Call SaveLnsInternalFiles || Goto done_error



Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem Rem
Echo ... 
Echo ... create all files needed by Install Shield for distribution
Echo .


:do_build_installers

Echo ************************************************
Echo.
Echo  Move files into the InstallShield build tree.
Echo.
Echo ************************************************
CD %BldPath%

Call PrepareLnsFiles
If ErrorLevel 1 Goto done_error

Echo ************************************************
Echo.
Echo  Build InstallShield Projects.
Echo.
Echo ************************************************
Set _PATH=%PATH%
Set PATH=%FastObjectsDir%\bin;%FastObjectsDir%\Runtime\bin;%PATH%

Call BuildLnsInstallers
If ErrorLevel 1 Goto done_error

Rem If debugging the build script, do not publish products!!!!
If Defined LnsBuildScriptDebug Goto done_noerror


Set SSDIR=%SSLnsSvr%
Set PATH=%_PATH%
Set _PATH=


:do_get_demoshield_files
CD %BldDir%

Rem Uncomment to skip publishing
Rem goto done_noerror

:do_publish_build

Echo ************************************************
Echo.
Echo  Publishing Build Results
Echo.
Echo ************************************************
Echo.
Echo.
Echo ... verMajor := %VerMajor%
Echo ... verMinor := %VerMinor%
Echo ... verBuild := %VerBuild%
Echo.
Echo.

Set _PATH=%PATH%
Set PATH=%FastObjectsDir%\bin;%FastObjectsDir%\Runtime\bin;%PATH%

Call SaveBuiltProducts
If ErrorLevel 1 Goto done_error

Set PATH=%_PATH%
Set _PATH=

Goto done_noerror


Rem     ---- SUBROUTINES -----------------------------------------------------

Rem     Call :BuildProduct {ProductName} [{BuildProject} [{BuildConfig}]] || Exit /B
:BuildProduct
    SetLocal
        Rem     Extract subroutine parameters
        Set ProductName=%~1
        Set BuildProject=%~2
        Set BuildConfig="%~3"
        
        Echo Building %ProductName%...
        
        Rem     Set unset parameters to their default values
        If "%BuildProject%" == "" Set BuildProject=_Build
        If  %BuildConfig%   == "" Set BuildConfig=%VSCfg%

        Rem     Import (from upstream) files, if an import spec exists
        p4 branches | Find /I "Branch Import_LNS_Dev_%ProductName% " > NUL && (
            Echo Importing %ProductName% upstream files...            
			Echo Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Import_LNS_Dev_%ProductName%.txt "Imported files for project located at '%git_lnsdev%\%ProductName%' for the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Import_LNS_Dev_%ProductName%"
			Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\Import_LNS_Dev_%ProductName%.txt "Imported files for project located at '%git_lnsdev%\%ProductName%' for the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:Import_LNS_Dev_%ProductName%"            
            Echo Done importing files for component %ProductName% ...
            Echo git sync ...
            Rem /SS git sync || Exit /B
            Echo.
        )


        Rem     Set Build Directories
        Set INCLUDE=%INCLUDE%;%STLportIncDir%
        Set INCLUDE=%INCLUDE%;%DevEnvDir%\AtlMfc\Include
        Set INCLUDE=%INCLUDE%;%DevEnvDir%\Include
        Set INCLUDE=%INCLUDE%;%PlatformSDKDir%\Include
        Set INCLUDE=%INCLUDE%;%FastObjectsDir%\Inc
        Set INCLUDE=%INCLUDE%;%BoostIncDir%
        Set INCLUDE=%INCLUDE%;%BldPath%\OBJ\Bin
        Set INCLUDE=%INCLUDE%;%WinKitPath%\8.1\Include\um;%WinKitPath%\8.1\Include\shared;%WinKitPath%\8.1\bin\x86
        Rem Echo INCLUDE=%INCLUDE%

        Set LIB=%STLportLibDir%
        Set LIB=%LIB%;%PlatformSDKDir%\Lib
        Set LIB=%LIB%;%DevEnvDir%\AtlMfc\Lib
        Set LIB=%LIB%;%DevEnvDir%\Lib
        Set LIB=%LIB%;%FastObjectsDir%\Lib
		set LIB=%LIB%;%WinKitPath%\8.1\Lib\winv6.3\um\x86
        If Defined BoostLibDir Set LIB=%LIB%;%BoostLibDir%
        Rem Echo LIB=%LIB%

        Set PATH=%BinDir%;%DevEnvBinDir%;%PlatformSDKDir%\Bin;%PlatformSDKDir%\Bin\WinNT;%DotNETPath%;%FastObjectsDir%\Bin;%FastObjectsDir%\Runtime\Bin;%PATH%;%WinKitPath%\8.1\Lib\winv6.3\um\x86;%WinKitPath%\8.1\bin\x86;%PlatformSDKDir%\bin\NETFX 4.5.1 Tools
        If Defined BoostBinDir Set PATH=%PATH%;%BoostBinDir%
        Rem Echo PATH=%PATH%


        Rem     Build Projects
        If Exist "%ProductName%.out" Del "%ProductName%.out"
       		rem DevEnv /Rebuild %BuildConfig% /Project %BuildProject% "%ProductName%.sln" /UseEnv /out "%ProductName%.out" || Exit /B
			DevEnv /Clean %BuildConfig% /Project %BuildProject% "%ProductName%.sln" /UseEnv || Exit /B
			DevEnv /Build %BuildConfig% /Project %BuildProject% "%ProductName%.sln" /UseEnv /out "%ProductName%.out" || Exit /B
        	Echo [Build Succeeded]


        Rem     Export (to Downstream) Files
        Rem Echo Exporting %ProductName% files...
        Rem Call p4Export -b "Export_LNS_Dev_%ProductName%_Source" -w "Export_LNS_Dev_%ProductName%_Target" || Exit /B
        Rem Echo.

    EndLocal
    Goto :EOF


Rem     ----------------------------------------------------------------------
:bad_syntax
    Echo ...
    Echo ... This will build a Version of OpenLNS SDK
    Echo ... The Version number must be specified on the cmnd line
    Echo ... The old build number must also be specified on the cmnd line
    Echo ...
    Echo ... syntax :
    Echo "...     mos <new build number> <old build number>"
    Echo "...     mos <old build number>"
    Echo ...
    Echo ... for example :
    Echo ...     mos 15 13
    Echo ...        - build 15 will use the dictionary from build 13, ...
    Echo ...     mos 13
    Echo ...        - the current build will use the dictionary from build 13 ...
    Echo .
    Set junkVal 1
    Goto done



Rem     ----------------------------------------------------------------------
:done_error
    Echo.
    Echo ***********************************************************************************
    Echo *** OpenLNS generated a real error!  Check build logs and .out files for cause. ***
    Echo ***********************************************************************************
    Echo.

    Rem     revert the changes in the changelist and delete the changelist
    If Not "%LabeledChanges%" == "" (
        If "%LabeledChangesDone%" == "0" (
            Set P4CLIENT=%P4LNSBUILD%
            Echo Reverting version changes ^(Workspace: %P4CLIENT%^)...
            Call %Revert% -c "%LabeledChanges%" //%P4CLIENT%/... > version_list_reverted.txt
            Type version_list_reverted.txt > %NUL%
            Call %ChangeDelete% "%LabeledChanges%"
        )
    )

    Set P4CLIENT=%P4LNSBUILD%
    If Defined ImportedChanges (
        Echo Reverting imports changelist #%ImportedChanges% ^(Workspace: %P4CLIENT%^)...
        Call %Revert% -c %ImportedChanges% //%P4CLIENT%/... > imported_list_reverted.txt
        Type imported_list_reverted.txt > %NUL%
        Call %ChangeDelete% %ImportedChanges%
        Set ImportedChanges=
    )

    Rem     if debugging the build script, skip the notification e-mail
    If Defined LnsBuildScriptDebug Goto exit_with_error

    Rem     if build is part of a compound build, skip the notification e-mail, as it may already be sent
    If Defined CompoundBuild Goto exit_with_error

        Call TimeIn 0 DoneTime

        If Not Defined SkipEmail (
            Rem	    send build failure notice via email
            Echo Sending build failure notice...
            blat - -subject "OpenLNS %ProductVersion% Build Failed" -to "%MailTo_Failure%" -priority 1 -q -body "The LNS %ProductVersion% build failed at %DoneTime%!||See error log at: \\%MachineName%\%BldDir%\Build\LnsBld.log (attached)" -attacht "\\%MachineName%\%BldDir%\Build\LnsBld.log" -debug -log blat.log
            If ErrorLevel 1 Echo NOTICE: Email notice delivery failed!
        )

        Set junkVal 1

:exit_with_error
    Rem set the error level for further builds if any and exit.
    Echo Exit with error
    Exit /B 1



Rem     ----------------------------------------------------------------------
:done_noerror
    If "%junkVal%" == "1" Goto done_error


    Rem Submit the export changelist
    Echo Checking in exported files...

    Rem Don't check in exports if debugging build script
    If Defined LnsBuildScriptDebug (
        If Not "%LabeledChanges%" == "" (
            If "%LabeledChangesDone%" == "0" (
                Set P4CLIENT=%P4LNSBUILD%
                Echo Debugging script: reverting version changes ^(Workspace: %git_lnsdev%^)...
				Rem /SS git restore > version_list_reverted.txt
                Type version_list_reverted.txt > %NUL%
                Rem /SS git opertion
            )
        )

        Set P4CLIENT=%P4LNSBUILD%
        If Defined ImportedChanges (
            Echo Reverting imports changelist #%ImportedChanges% ^(Workspace: %git_lnsdev%^)...
            Rem /SS git restore > imported_list_reverted.txt
            Type imported_list_reverted.txt > %NUL%
            Rem /SS git opertion
            Set ImportedChanges=
        )

        Goto exports_checkin_done
    )

    Set P4CLIENT=%P4LNSBUILD%
    If Defined ImportedChanges (
        Echo Submitting imports changelist #%ImportedChanges% ^(Workspace: %git_lnsdev%^)...
        Rem /SS add/commit/push
        Set ImportedChanges=
    )

    Echo Exporting OpenLNS source and binaries for external projects to //depot/Software/Export/LNS/Dev folder
	Echo Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\%P4LNSEXPORTBRANCH%.txt "Exports from OpenLNS Build %ProductVersion%. Exported file from project located at '%git_lnsdev%' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:%P4LNSEXPORTBRANCH%" || Goto done_Error
	Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\%P4LNSEXPORTBRANCH%.txt "Exports from OpenLNS Build %ProductVersion%. Exported file from project located at '%git_lnsdev%' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:%P4LNSEXPORTBRANCH%" || Goto done_Error
	Echo Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\%P4LNSEXPORTWORKSPACE%.txt "Exports from OpenLNS Build %ProductVersion%. Exported file from project located at '%git_lnsdev%' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:%P4LNSEXPORTWORKSPACE%" || Goto done_Error
	Call %BinDir%\GIT_Import_Export.bat %BldPath%\BuildViews\%P4LNSEXPORTWORKSPACE%.txt "Exports from OpenLNS Build %ProductVersion%. Exported file from project located at '%git_lnsdev%' of the branch '%git_lnsdev_branch%' on host '%MachineName%'. View file used:%P4LNSEXPORTWORKSPACE%" || Goto done_Error

    :exports_checkin_done

    Echo ...................................................
    Echo ...................................................
    Echo ... OpenLNS Build Complete with no fatal errors ...
    Echo ...................................................
    Echo ...................................................
    Echo.

    Rem If debugging the build script, skip the notification e-mail
    If Defined LnsBuildScriptDebug Goto done

    Rem If build is part of a compound build, skip the notification e-mail, as it may already be sent
    If Defined CompoundBuild Goto done

    Call TimeIn 0 DoneTime

    If Not Defined SkipEmail (
        Rem	Send build success notice via email.
        Echo Sending build completion notice...
        blat - -subject "OpenLNS %ProductVersion% Build Complete" -to "%MailTo_Success%" -q -body "The LNS %ProductVersion% build completed successfully at %DoneTime% and is now ready for Smoke Test." -debug -log blat.log
        If ErrorLevel 1 Echo NOTICE: Email notice delivery failed!
    )

:done
