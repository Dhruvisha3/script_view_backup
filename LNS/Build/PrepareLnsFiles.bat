@echo off

Rem     $Header$
Rem
Rem     (c) Copyright 2004-2012, Echelon Corporation.  All rights reserved.

@echo ..
@echo ..  !!   							                                !!
@echo ..  !!   Move Built files from build directories to locations     !!
@echo ..  !!   that mirror the installed images of the various OpenLNS  !!
@echo ..  !!   products, including the OpenLNS Server and OpenLNS SDK.  !!
@echo ..  !!   							                                !!
@echo ..

rem .
rem . set date/time stamps on all files
rem . - requires copying all files to editable directories
rem . - each install component (and nested component) needs unique directory
rem .

@echo .. Getting version info file from NSS ..
pushd %BldPath%
call getlnsver.bat -e -f import\version.h
popd

Set LnsServerDataDir=%IsProjPath%\LNS_Server\Data
Set LnsServerSilentDataDir=%IsProjPath%\LNS_Server_Silent\Data
Set LnsSdkDataDir=%IsProjPath%\LNS_ADK\Data
Set BinaryConvertersDataDir=%IsProjPath%\BinaryConverters\Data
Set LnsPrimaryInteropAssemblyDataDir=%IsProjPath%\LnsPrimaryInteropAssembly\Data

@echo ..
@echo ..  Delete all target directories prior to moving files ..
@echo ..
pushd %IsBldDrive%\
Rem Delete the last data directories to eliminate noise from the last build
rmdir /S /Q %LnsServerDataDir%
rmdir /S /Q %LnsServerSilentDataDir%
rmdir /S /Q %LnsSdkDataDir%
rmdir /S /Q %BinaryConvertersDataDir%
rmdir /S /Q %LnsPrimaryInteropAssemblyDataDir%

@echo ..
@echo ..  Make all of the editable directories
md %LnsServerDataDir%
mklink /D %LnsServerSilentDataDir% %LnsServerDataDir%
md %LnsSdkDataDir%
md %BinaryConvertersDataDir%
md %LnsPrimaryInteropAssemblyDataDir%

@echo ..  Make directories for the Binary Conversion utilities ..
md "%BinaryConvertersDataDir%\bin"

@echo ..  Make directories for the Primary Interop Assembly files ..
md "%LnsPrimaryInteropAssemblyDataDir%\GAC"

@echo ..  Make directories for the OpenLNS Server ..

md "%LnsServerDataDir%\bin"
md "%LnsServerDataDir%\bin"
md "%LnsServerDataDir%\Documentation"
md "%LnsServerDataDir%\export"
md "%LnsServerDataDir%\import"
md "%LnsServerDataDir%\NetworkServices"
md "%LnsServerDataDir%\NetworkServices\DbRev"
md "%LnsServerDataDir%\NetworkServices\DbRev\Lns_1.0"
md "%LnsServerDataDir%\NetworkServices\DbRev\Lns_3.0"
md "%LnsServerDataDir%\NetworkServices\DbRev\Lns_3.0.prev"
md "%LnsServerDataDir%\NetworkServices\DbRev\Lns_3.2"
md "%LnsServerDataDir%\NetworkServices\DbRev\Lns_4.0"
md "%LnsServerDataDir%\NetworkServices\SystemImage"
md "%LnsServerDataDir%\ObjectServer"
md "%LnsServerDataDir%\ObjectServer\Dictionary"
md "%LnsServerDataDir%\ObjectServer\GlobalDb"
md "%LnsServerDataDir%\ObjectServer\GlobalDb\recovery"
md "%LnsServerDataDir%\ObjectServer\BackupDb"
md "%LnsServerDataDir%\ObjectServer\BackupDb\recovery"
md "%LnsServerDataDir%\ObjectServer\Assemblies"
md "%LnsServerDataDir%\types"
md "%LnsServerDataDir%\Win"
md "%LnsServerDataDir%\Win\LNS Licenses"
md "%LnsServerDataDir%\Installer"
md "%LnsServerDataDir%\Installer\SetupDir"
md "%LnsServerDataDir%\Installer\TempFiles"
md "%LnsServerDataDir%\Installer\TempFiles\Resources"
md "%LnsServerDataDir%\Installer\TempFiles\RegFiles"
md "%LnsServerDataDir%\Installer\TempFiles\HelperDLLs"
md "%LnsServerDataDir%\Installer\TempFiles\EmbeddedInstallers"
md "%LnsServerDataDir%\Installer\TempFiles\License"
md "%LnsServerDataDir%\Installer\TempFiles\Scripts"

@echo ..  Make directories for the OpenLNS Server DEBUG runtime ..
md "%LnsServerDataDir%\DEBUG\bin"

@echo ..  Make directories for the OpenLNS SDK runtime ..
md "%LnsSdkDataDir%\bin"
md "%LnsSdkDataDir%\Documentation"
md "%LnsSdkDataDir%\ObjectServer"
md "%LnsSdkDataDir%\ObjectServer\Include"
md "%LnsSdkDataDir%\ObjectServer\Examples"
md "%LnsSdkDataDir%\ObjectServer\Examples\CommonComponents"
md "%LnsSdkDataDir%\ObjectServer\Examples\MonitorAndControl"
md "%LnsSdkDataDir%\ObjectServer\Examples\NetworkManagement"
md "%LnsSdkDataDir%\ObjectServer\Examples\PluginDirector"
md "%LnsSdkDataDir%\ObjectServer\Examples\Import"
md "%LnsSdkDataDir%\ObjectServer\Examples\NcSource"
md "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\LampActuator"
md "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\LightSwitch"
md "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\LightSwitchingThermostat"
md "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\Thermostat"
md "%LnsSdkDataDir%\Installer"
md "%LnsSdkDataDir%\Installer\SetupDir"
md "%LnsSdkDataDir%\Installer\SetupDir\AdobeAcrobat"
md "%LnsSdkDataDir%\Installer\TempFiles"
md "%LnsSdkDataDir%\Installer\TempFiles\License"
md "%LnsSdkDataDir%\Installer\TempFiles\RegFiles"
md "%LnsSdkDataDir%\Installer\TempFiles\HelperDLLs"
md "%LnsSdkDataDir%\Installer\TempFiles\Resources"
md "%LnsSdkDataDir%\LonWorks"

@echo ..  BEGIN:  Getting Example Files for the OpenLNS SDK ..
copy %BldPath%\Examples\LcaDirector-VB6\*.cls  "%LnsSdkDataDir%\ObjectServer\Examples\PluginDirector" > %NUL%
copy %BldPath%\Examples\LcaDirector-VB6\*.frm  "%LnsSdkDataDir%\ObjectServer\Examples\PluginDirector" > %NUL%
copy %BldPath%\Examples\LcaDirector-VB6\*.frx  "%LnsSdkDataDir%\ObjectServer\Examples\PluginDirector" > %NUL%
copy "%BldPath%\Examples\LcaDirector-VB6\LNS Director Example.exe"  "%LnsSdkDataDir%\ObjectServer\Examples\PluginDirector" > %NUL%
copy "%BldPath%\Examples\LcaDirector-VB6\LNS Director Example.vbp"  "%LnsSdkDataDir%\ObjectServer\Examples\PluginDirector" > %NUL%
copy "%BldPath%\Examples\LcaDirector-VB6\LNS Director Example.vbw"  "%LnsSdkDataDir%\ObjectServer\Examples\PluginDirector" > %NUL%
attrib -r "%LnsSdkDataDir%\ObjectServer\Examples\PluginDirector\*.*" /s
setfdate * %vermajor%:%verminor%:0 "%LnsSdkDataDir%\ObjectServer\Examples\PluginDirector\*.*" > %NUL%

copy %BldPath%\Examples\LcaMonitor\*.h  "%LnsSdkDataDir%\ObjectServer\Examples\MonitorAndControl" > %NUL%
copy %BldPath%\Examples\LcaMonitor\*.cpp  "%LnsSdkDataDir%\ObjectServer\Examples\MonitorAndControl" > %NUL%
copy %BldPath%\Examples\LcaMonitor\*.rc  "%LnsSdkDataDir%\ObjectServer\Examples\MonitorAndControl" > %NUL%
copy %BldPath%\Examples\LcaMonitor\*.rc2  "%LnsSdkDataDir%\ObjectServer\Examples\MonitorAndControl" > %NUL%
copy %BldPath%\Examples\LcaMonitor\*.ico  "%LnsSdkDataDir%\ObjectServer\Examples\MonitorAndControl" > %NUL%
copy %BldPath%\Examples\LcaMonitor\LcaMonitor.dsp  "%LnsSdkDataDir%\ObjectServer\Examples\MonitorAndControl" > %NUL%
copy %BldPath%\Examples\LcaMonitor\LcaMonitor.dsw  "%LnsSdkDataDir%\ObjectServer\Examples\MonitorAndControl" > %NUL%
copy %BldPath%\Examples\LcaMonitor\LcaMonitor.sln  "%LnsSdkDataDir%\ObjectServer\Examples\MonitorAndControl" > %NUL%
copy %BldPath%\Examples\LcaMonitor\LcaMonitor.vcproj  "%LnsSdkDataDir%\ObjectServer\Examples\MonitorAndControl" > %NUL%
copy %BldPath%\Examples\LcaMonitor\LcaMonitor.exe  "%LnsSdkDataDir%\ObjectServer\Examples\MonitorAndControl" > %NUL%
attrib -r "%LnsSdkDataDir%\ObjectServer\Examples\MonitorAndControl\*.*" /s
rem Remove the link to the source safe control from the project file
call StripSccInfo -f "%LnsSdkDataDir%\ObjectServer\Examples\MonitorAndControl\LcaMonitor.dsp"
erase "%LnsSdkDataDir%\ObjectServer\Examples\MonitorAndControl\LcaMonitor.bak"
setfdate * %vermajor%:%verminor%:0 "%LnsSdkDataDir%\ObjectServer\Examples\MonitorAndControl\*.*" > %NUL%

copy %BldPath%\Examples\LcaNetMgmt\*.h  "%LnsSdkDataDir%\ObjectServer\Examples\NetworkManagement" > %NUL%
copy %BldPath%\Examples\LcaNetMgmt\*.cpp  "%LnsSdkDataDir%\ObjectServer\Examples\NetworkManagement" > %NUL%
copy %BldPath%\Examples\LcaNetMgmt\*.rc  "%LnsSdkDataDir%\ObjectServer\Examples\NetworkManagement" > %NUL%
copy %BldPath%\Examples\LcaNetMgmt\*.rc2  "%LnsSdkDataDir%\ObjectServer\Examples\NetworkManagement" > %NUL%
copy %BldPath%\Examples\LcaNetMgmt\*.ico  "%LnsSdkDataDir%\ObjectServer\Examples\NetworkManagement" > %NUL%
copy %BldPath%\Examples\LcaNetMgmt\LcaNetMgmt.dsp  "%LnsSdkDataDir%\ObjectServer\Examples\NetworkManagement" > %NUL%
copy %BldPath%\Examples\LcaNetMgmt\LcaNetMgmt.dsw  "%LnsSdkDataDir%\ObjectServer\Examples\NetworkManagement" > %NUL%
copy %BldPath%\Examples\LcaNetMgmt\LcaNetMgmt.sln  "%LnsSdkDataDir%\ObjectServer\Examples\NetworkManagement" > %NUL%
copy %BldPath%\Examples\LcaNetMgmt\LcaNetMgmt.vcproj  "%LnsSdkDataDir%\ObjectServer\Examples\NetworkManagement" > %NUL%
copy %BldPath%\Examples\LcaNetMgmt\Release\LcaNetMgmt.exe  "%LnsSdkDataDir%\ObjectServer\Examples\NetworkManagement" > %NUL%
attrib -r "%LnsSdkDataDir%\ObjectServer\Examples\NetworkManagement\*.*" /s
rem Remove the link to the source safe control from the project file
call StripSccInfo -f "%LnsSdkDataDir%\ObjectServer\Examples\NetworkManagement\LcaNetMgmt.dsp"
erase "%LnsSdkDataDir%\ObjectServer\Examples\NetworkManagement\LcaNetMgmt.bak"
setfdate * %vermajor%:%verminor%:0 "%LnsSdkDataDir%\ObjectServer\Examples\NetworkManagement\*.*" > %NUL%

rem Get the Tree View control used by the preceding three examples
copy %BldPath%\Examples\Controls\LNSTreeView.ocx  "%LnsSdkDataDir%\ObjectServer\Examples\CommonComponents" > %NUL%
attrib -r "%LnsSdkDataDir%\ObjectServer\Examples\CommonComponents\*.*" /s
setfdate * %vermajor%:%verminor%:0 "%LnsSdkDataDir%\ObjectServer\Examples\CommonComponents\*.*" > %NUL%

rem Get the Device-specific files (XIF, etc) used by the SDK Examples
copy %BldPath%\Examples\Import\*.APB  "%LnsSdkDataDir%\ObjectServer\Examples\Import" > %NUL%
copy %BldPath%\Examples\Import\*.XFB  "%LnsSdkDataDir%\ObjectServer\Examples\Import" > %NUL%
copy %BldPath%\Examples\Import\*.XIF  "%LnsSdkDataDir%\ObjectServer\Examples\Import" > %NUL%
attrib -r "%LnsSdkDataDir%\ObjectServer\Examples\Import\*.*" /s
setfdate * %vermajor%:%verminor%:0 "%LnsSdkDataDir%\ObjectServer\Examples\Import\*.*" > %NUL%

rem Get the Neuron C source code for the Neuron C examples used for the OpenLNS SDK examples

copy "%BldPath%\Examples\NB31Prj_LNS32Devices\Lamp Actuator\*.*" "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\LampActuator"
attrib -r "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\LampActuator\*.*" /s
setfdate * %vermajor%:%verminor%:0 "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\LampActuator\*.*" > %NUL%
erase "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\LampActuator\vssver.scc" 2>NUL

copy "%BldPath%\Examples\NB31Prj_LNS32Devices\Light Switch\*.*" "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\LightSwitch"
attrib -r "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\LightSwitch\*.*" /s
setfdate * %vermajor%:%verminor%:0 "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\LightSwitch\*.*" > %NUL%
erase "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\LightSwitch\vssver.scc" 2>NUL

copy "%BldPath%\Examples\NB31Prj_LNS32Devices\Light Switching Thermostat\*.*" "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\LightSwitchingThermostat"
attrib -r "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\LightSwitchingThermostat\*.*" /s
setfdate * %vermajor%:%verminor%:0 "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\LightSwitchingThermostat\*.*" > %NUL%
erase "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\LightSwitchingThermostat\vssver.scc" 2>NUL

copy "%BldPath%\Examples\NB31Prj_LNS32Devices\Thermostat\*.*" "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\Thermostat"
attrib -r "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\Thermostat\*.*" /s
setfdate * %vermajor%:%verminor%:0 "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\Thermostat\*.*" > %NUL%
erase "%LnsSdkDataDir%\ObjectServer\Examples\NcSource\Thermostat\vssver.scc" 2>NUL

@echo ..  END:  Getting Example Files for the OpenLNS SDK ..

@echo ..  BEGIN:  Getting Files for NSS ..

copy %BldPath%\nss\NetworkServices\DbRev\*.dbd "%LnsServerDataDir%\NetworkServices\DbRev" > %NUL%
copy %BldPath%\nss\NetworkServices\DbRev\Lns_1.0\*.dbd "%LnsServerDataDir%\NetworkServices\DbRev\Lns_1.0" > %NUL%
copy %BldPath%\nss\NetworkServices\DbRev\Lns_3.0\*.rdl "%LnsServerDataDir%\NetworkServices\DbRev\Lns_3.0" > %NUL%
copy %BldPath%\nss\NetworkServices\DbRev\Lns_3.0\*.dbd "%LnsServerDataDir%\NetworkServices\DbRev\Lns_3.0" > %NUL%
copy %BldPath%\nss\NetworkServices\DbRev\Lns_3.0.prev\*.rdl "%LnsServerDataDir%\NetworkServices\DbRev\Lns_3.0.prev" > %NUL%
copy %BldPath%\nss\NetworkServices\DbRev\Lns_3.0.prev\*.dbd "%LnsServerDataDir%\NetworkServices\DbRev\Lns_3.0.prev" > %NUL%
copy %BldPath%\nss\NetworkServices\DbRev\Lns_3.2\*.rdl "%LnsServerDataDir%\NetworkServices\DbRev\Lns_3.2" > %NUL%
copy %BldPath%\nss\NetworkServices\DbRev\Lns_3.2\*.dbd "%LnsServerDataDir%\NetworkServices\DbRev\Lns_3.2" > %NUL%
copy %BldPath%\nss\NetworkServices\DbRev\Lns_4.0\*.rdl "%LnsServerDataDir%\NetworkServices\DbRev\Lns_4.0" > %NUL%
copy %BldPath%\nss\NetworkServices\DbRev\Lns_4.0\*.dbd "%LnsServerDataDir%\NetworkServices\DbRev\Lns_4.0" > %NUL%
attrib -r "%LnsServerDataDir%\NetworkServices\DbRev\*.*" /s
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\NetworkServices\DbRev\*.*" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\NetworkServices\DbRev\Lns_1.0\*.*" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\NetworkServices\DbRev\Lns_3.0\*.*" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\NetworkServices\DbRev\Lns_3.0.prev\*.*" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\NetworkServices\DbRev\Lns_3.2\*.*" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\NetworkServices\DbRev\Lns_4.0\*.*" > %NUL%

copy %BldPath%\nss\NetworkServices\SystemImage\*.DevPi "%LnsServerDataDir%\NetworkServices\SystemImage" > %NUL%
copy %BldPath%\nss\NetworkServices\SystemImage\*.apb "%LnsServerDataDir%\NetworkServices\SystemImage" > %NUL%
attrib -r "%LnsServerDataDir%\NetworkServices\SystemImage\*.*" /s
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\NetworkServices\SystemImage\*.*" > %NUL%

rem ins20\nsseng.exe is a signature file, not an executable
copy %BldPath%\nss\ins20\nsseng.exe    "%LnsServerDataDir%\Win\LNS Licenses" > %NUL%

copy %BldPath%\nss\bin\m*.dbd          "%LnsServerDataDir%\bin"    > %NUL%
copy %BldPath%\nss\bin\nsscore.dll     "%LnsServerDataDir%\bin"    > %NUL%
copy %BldPath%\nss\bin\nssdbg.exe      "%LnsServerDataDir%\bin"    > %NUL%
copy %BldPath%\nss\bin\nssmch.exe      "%LnsServerDataDir%\bin"    > %NUL%
copy %BldPath%\nss\bin\nssrdm32.dll    "%LnsServerDataDir%\bin"    > %NUL%
copy %BldPath%\nss\bin\nsstrace.exe    "%LnsServerDataDir%\bin"    > %NUL%
copy %BldPath%\nss\bin\nsseng.exe      "%LnsServerDataDir%\bin"    > %NUL%
copy %BldPath%\nss\bin\nsstst32.dat    "%LnsServerDataDir%\bin"    > %NUL%
copy %BldPath%\nss\bin\nsstst32.exe    "%LnsServerDataDir%\bin"    > %NUL%
copy %BldPath%\nss\bin\NssTraceSupport.dll "%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\DevProgInfoXml.dll "%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\nssipc32.dll      "%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\LnsProtocolMode.exe            "%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\LonTalkIpAddressCalculator.exe "%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\LonTalkIpInterfaces.exe        "%LnsServerDataDir%\bin" > %NUL%

copy %BldPath%\nss\bin\nxe32bin.exe 	"%BinaryConvertersDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\xif32bin.exe 	"%BinaryConvertersDataDir%\bin" > %NUL%

copy %BldPath%\nss\bin\ni32.dll     	"%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\ns32.dll     	"%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\ns32lic.dll  	"%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\nse_rcv.dll  	"%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\nsicore.dll  	"%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\nsieng.exe   	"%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\nsseve.dll   	"%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\dbrev.exe    	"%LnsServerDataDir%\bin" > %NUL%

Rem This is for CrypKey compatibility functions
copy %BldPath%\nss\export\crp32dll.dll	"%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\import\LicenseCompatibility\checklic.exe	"%LnsServerDataDir%\bin" > %NUL%

copy %BldPath%\lca\xif\router.*    "%LnsServerDataDir%\Import" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\Import\*.*" > %NUL%

@echo ..  END:  Getting Files for NSS ..


@echo ..  BEGIN:  Getting ObjectServer Files ..

copy %BldPath%\obj\bin\lcaeng.dll      "%LnsServerDataDir%\bin" > %NUL%

copy %BldPath%\obj\bin\lcapjm32.dll    "%LnsServerDataDir%\bin" > %NUL%

copy %BldPath%\obj\bin\typedb.dbd      "%LnsServerDataDir%\bin" > %NUL%

copy %BldPath%\DasVni\bin\LcaDatSv.dll "%LnsServerDataDir%\bin" > %NUL%

rem If debug build, the LnsDs.dll was not built, so get the obj\bin archive copy instead
if "%LnsBuildDebugMode%" == "TRUE" Goto copy_Archived_LnsDs
copy %BldPath%\DasVni\bin\LnsDs.dll    "%LnsServerDataDir%\bin" > %NUL%
:copy_Archived_LnsDs
copy %BldPath%\obj\bin\LnsDs.dll       "%LnsServerDataDir%\bin" > %NUL%

copy %BldPath%\Formatter\bin\lonfmt.dll       "%LnsServerDataDir%\bin"   > %NUL%
copy %BldPath%\Formatter\bin\lnsformat.dll    "%LnsServerDataDir%\bin"   > %NUL%

copy %BldPath%\obj\bin\lcastrsv.dll    "%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\obj\bin\lcastrsvfra.dll "%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\obj\bin\lcastrsvdeu.dll "%LnsServerDataDir%\bin" > %NUL%

copy %BldPath%\obj\bin\lcaobjsv.ocx    "%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\obj\ocx\lcaobjsv.lic    "%LnsSdkDataDir%\bin"  > %NUL%

copy %BldPath%\obj\bin\regex.dll       "%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\obj\bin\lcaserv.exe     "%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\obj\bin\lcasvfra.dll    "%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\obj\bin\lcasvdeu.dll    "%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\obj\bin\lcamon.exe      "%LnsServerDataDir%\bin" > %NUL%

copy %BldPath%\Import\Microsoft\srvany.exe    "%LnsServerDataDir%\bin" > %NUL%

copy %BldPath%\obj\bin\LcaObjSvPS.dll  "%LnsServerDataDir%\bin" > %NUL%

copy %BldPath%\obj\bin\LnsLog.dll      "%LnsServerDataDir%\bin" > %NUL%

copy %BldPath%\obj\bin\ptserver400.cfg "%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\obj\bin\lcaobjsv400.cfg "%LnsServerDataDir%\bin" > %NUL%

copy %BldPath%\obj\poedb\dictionary\_objects.*  "%LnsServerDataDir%\ObjectServer\Dictionary" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\ObjectServer\Dictionary\*.*"       > %NUL%

copy %BldPath%\obj\poedb\base\objects.*  "%LnsServerDataDir%\ObjectServer\GlobalDb" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\ObjectServer\GlobalDb\*.*"  > %NUL%

copy %BldPath%\obj\poedb\base\recovery\data0000.rcy  "%LnsServerDataDir%\ObjectServer\GlobalDb\recovery" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\ObjectServer\GlobalDb\recovery\*.*" > %NUL%

copy %BldPath%\obj\poedb\base\objects.*  "%LnsServerDataDir%\ObjectServer\BackupDb" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\ObjectServer\BackupDb\*.*"  > %NUL%

copy %BldPath%\obj\poedb\base\recovery\data0000.rcy  "%LnsServerDataDir%\ObjectServer\BackupDb\recovery" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\ObjectServer\BackupDb\recovery\*.*" > %NUL%

rem Copy the FastObjects runtime files
copy "%BldPath%\Import\FastObjects\Bin\Release\*.exe"     "%LnsServerDataDir%\bin" > %NUL%
copy "%BldPath%\Import\FastObjects\Bin\Release\*.dll"     "%LnsServerDataDir%\bin" > %NUL%
copy "%BldPath%\Import\FastObjects\Bin\Release\license"   "%LnsServerDataDir%\bin" > %NUL%

rem Copy the release mode STLport dll
copy %BldPath%\Import\bin\Release\stlport_vc12_echelon.5.2.dll	"%LnsServerDataDir%\bin" > %NUL%

@echo ..  END:  Getting ObjectServer Files ..


@echo ..  BEGIN: Getting the Primary Interop Assembly files   ..

copy %BldPath%\obj\bin\Echelon.LNS.Interop.dll      		"%LnsPrimaryInteropAssemblyDataDir%\GAC" > %NUL%
copy %BldPath%\obj\bin\Echelon.LNS.Interop.*.config      	"%LnsPrimaryInteropAssemblyDataDir%\GAC" > %NUL%
copy %BldPath%\obj\bin\Policy.*Echelon.LNS.Interop.dll      "%LnsPrimaryInteropAssemblyDataDir%\GAC" > %NUL%

rem Install another copy of the PIA for software developers
copy %BldPath%\obj\bin\Echelon.LNS.Interop.dll      		"%LnsServerDataDir%\ObjectServer\Assemblies" > %NUL%

setfdate * %vermajor%:%verminor%:0 "%LnsPrimaryInteropAssemblyDataDir%\GAC" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\ObjectServer\Assemblies" > %NUL%

@echo ..  END: Getting the Primary Interop Assembly files   ..


@echo ..  BEGIN: Getting the Utility Applications   ..

rem use this About Box graphic for all OpenLNS utility apps
copy %BldPath%\Graphics\OpenLnsAboutBox.bmp    "%LnsServerDataDir%\bin" > %NUL%

copy %BldPath%\lca\bin\LnsDbValidationTool.exe "%LnsServerDataDir%\bin" > %NUL%

copy "%BldPath%\Europe\LNS Object Browser\Distributables\LnsObjectBrowser.exe" "%LnsServerDataDir%\bin" > %NUL%

Rem     OpenLNS Database Recovery Wizard
PushD "%BldPath%\Europe\LnsDbWizard\Bin"
    Copy "LNS Database Recovery Wizard.exe"     "%LnsServerDataDir%\bin" > %NUL%
    Copy Echelon.LNS.DbWizard.Interop.dll       "%LnsServerDataDir%\bin" > %NUL%
    Copy LnsDbWizard.dll                        "%LnsServerDataDir%\bin" > %NUL%
    Copy LnsDbWizardDEU.dll                     "%LnsServerDataDir%\bin" > %NUL%
    Copy LnsDbWizardFRA.dll                     "%LnsServerDataDir%\bin" > %NUL%
PopD

SetFDate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\bin\*.*" > %NUL%

@echo ..  END: Getting the Utility Applications   ..


@echo ..  BEGIN: Getting the help files   ..

copy "%BldPath%\documentation\help files\OpenLNS Programmers Reference.pdf" "%LnsSdkDataDir%\Documentation" > %NUL%
copy "%BldPath%\documentation\help files\OpenLNS Programmers Reference.pdf" "%LnsServerDataDir%\Documentation" > %NUL%
copy "%BldPath%\documentation\help files\LNS Programmers Guide.pdf"  "%LnsSdkDataDir%\Documentation" > %NUL%
copy "%BldPath%\documentation\help files\LNS Programmers Guide.pdf"  "%LnsServerDataDir%\Documentation" > %NUL%

copy "%BldPath%\documentation\help files\LcaServ.chm" "%LnsServerDataDir%\bin" > %NUL%
copy "%BldPath%\documentation\help files\LnsObjBr.chm" "%LnsServerDataDir%\bin" > %NUL%
copy "%BldPath%\documentation\help files\LnsDbValidationTool.chm" "%LnsServerDataDir%\bin" > %NUL%

setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\bin\*.chm" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\Documentation\*.pdf" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsSdkDataDir%\Documentation\*.pdf" > %NUL%

@echo ..  END: Getting the help files   ..


@echo ..  BEGIN: Getting the Type Library files   ..

copy %BldPath%\obj\bin\lcastrsv.tlb "%LnsServerDataDir%\bin" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\bin\lcastrsv.tlb" > %NUL%

@echo ..  END: Getting the Type Library files   ..


@echo ..  BEGIN: Getting the Installation Helper files   ..

copy %BldPath%\Graphics\OpenLNS.ico     "%LnsSdkDataDir%\Installer\SetupDir" > %NUL%
copy %BldPath%\Graphics\OpenLNS.ico     "%LnsServerDataDir%\Installer\SetupDir" > %NUL%
copy %BldPath%\documentation\ServerReadMe.htm   "%LnsServerDataDir%\ObjectServer\ReadMe.htm" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsSdkDataDir%\Installer\SetupDir\*.*" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\ObjectServer\ReadMe.htm" > %NUL%

copy "%BldPath%\Lca\ins\ADK License.rtf" "%LnsSdkDataDir%\Installer\TempFiles\License\License.rtf" > %NUL%
Rem copy "%BldPath%\Lca\ins\ADK License.rtf" "%LnsServerDataDir%\Installer\TempFiles\License\License.rtf" > %NUL%

copy %BldPath%\Import\Installers\_SetupXsoMinXP.dll "%LnsServerDataDir%\Installer\TempFiles\HelperDLLs" > %NUL%

copy "%BldPath%\Install\bin\SetLnsServerFirewallAccess.vbs" "%LnsServerDataDir%\Installer\TempFiles\Scripts" > %NUL%

copy "%BldPath%\Import\Installers\AdbeRdr920_en_us.exe" "%LnsSdkDataDir%\Installer\SetupDir\AdobeAcrobat" > %NUL%
copy "%BldPath%\Import\Installers\LonMarkResourceFiles1600.msi" "%LnsServerDataDir%\Installer\Tempfiles\EmbeddedInstallers" > %NUL%
copy "%BldPath%\Import\Installers\OpenLDV510.msi" "%LnsServerDataDir%\Installer\TempFiles\EmbeddedInstallers" > %NUL%
copy "%BldPath%\Import\Installers\msxml6.msi" "%LnsServerDataDir%\Installer\TempFiles\EmbeddedInstallers" > %NUL%
copy "%BldPath%\Import\Installers\Ip852ConfigServer401.msi" "%LnsServerDataDir%\Installer\TempFiles\EmbeddedInstallers" > %NUL%
copy "%BldPath%\Import\Installers\LicenseWizard100.msi" "%LnsServerDataDir%\Installer\TempFiles\EmbeddedInstallers" > %NUL%
copy "%BldPath%\Import\Installers\LicenseWizard100.exe" "%LnsServerDataDir%\Installer\TempFiles\EmbeddedInstallers" > %NUL%

rem copy "%BldPath%\Import\Installers\OpenLDV400-ADK.msi" "%LnsSdkDataDir%\Installer\TempFiles\EmbeddedInstallers" > %NUL%

copy %BldPath%\Graphics\LnsInstallMain.bmp     	"%LnsServerDataDir%\Installer\TempFiles\Resources" > %NUL%
copy %BldPath%\Graphics\LnsInstallBanner.bmp	"%LnsServerDataDir%\Installer\TempFiles\Resources" > %NUL%
copy %BldPath%\Graphics\LnsSdkInstallMain.bmp	"%LnsSdkDataDir%\Installer\TempFiles\Resources" > %NUL%
copy %BldPath%\Graphics\LnsSdkInstallBanner.bmp	"%LnsSdkDataDir%\Installer\TempFiles\Resources" > %NUL%

rem remove read-only attribute from some files to be installed in the clear
attrib -r "%LnsSdkDataDir%\Installer\SetupDir"

@echo ..  END: Getting the Installation Helper files   ..


@echo ..  BEGIN: Moving files to allow for Install build-time collection of COM registration info   ..
@echo ..  IMPORTANT:  To satisfy this, you are required to install OpenLDV on the OpenLNS build machine   ..

copy "%BldPath%\Import\ldrf\Release\*.dll"			"%LnsServerDataDir%\bin" > %NUL%

rem copy necessary VNI and VxLayer files from NSS\bin
copy %BldPath%\nss\bin\vxlayer.dll   				"%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\EchelonIpc.dll   			"%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\vnibase.dll   				"%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\vniclient.dll   				"%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\vniserver.exe   				"%LnsServerDataDir%\bin" > %NUL%
copy %BldPath%\nss\bin\vnistack.dll   				"%LnsServerDataDir%\bin" > %NUL%

@echo ..  END: Moving files to allow for Install build-time collection of COM registration info   ..


@echo ..  BEGIN: Getting the DEBUG mode files  ..

if "%LnsBuildDebugMode%" == "FALSE" Goto dont_copy_debug_files

rem The Data Server engine has a different name in debug mode
copy %BldPath%\DasVni\bin\LnsDsD.dll      	 "%LnsServerDataDir%\DEBUG\bin" > %NUL%
copy %BldPath%\obj\bin\LnsDs.dll      	 	 "%LnsServerDataDir%\DEBUG\bin" > %NUL%

rem Copy the debug mode STLport dll and pdb files
copy %BldPath%\Import\bin\Debug\*.dll	"%LnsServerDataDir%\DEBUG\bin" > %NUL%
copy %BldPath%\Import\bin\Debug\*.pdb	"%LnsServerDataDir%\DEBUG\bin" > %NUL%

rem The POET runtime files have different names in debug mode
copy "%BldPath%\Import\FastObjects\Bin\Debug\*.exe"     "%LnsServerDataDir%\DEBUG\bin" > %NUL%
copy "%BldPath%\Import\FastObjects\Bin\Debug\*.dll"     "%LnsServerDataDir%\DEBUG\bin" > %NUL%

rem Copy these real runtime files to \LNS Server\bin as well, so that all
rem dependencies are resolved during the build of the DEBUG runtime installers
rem (the installation build scans some components for COM registration)
copy "%LnsServerDataDir%\DEBUG\bin\*.exe" "%LnsServerDataDir%\bin" > %NUL%
copy "%LnsServerDataDir%\DEBUG\bin\*.dll" "%LnsServerDataDir%\bin" > %NUL%

rem Get PDBs for the rest of the runtime
For %%d In (NSS DASVNI OBJ LCA) Do Call :CopyPDB %%d

setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\DEBUG\bin\*.*" > %NUL%

:dont_copy_debug_files

@echo ..  END: Getting the DEBUG mode files  ..

@echo ..  BEGIN: Time-stamping the BIN directories.  First, give the    ..
@echo ..         generic release time-and-date stamp, then stamp files  ..
@echo ..         that have a time stamp based on another product.       ..

setfdate * %vermajor%:%verminor%:0 "%LnsServerDataDir%\bin\*.*" > %NUL%
setfdate * %vermajor%:%verminor%:0 "%LnsSdkDataDir%\bin\*.*" > %NUL%

@echo ..  END: Time-stamping the BIN directories   ..

Goto done

Rem  --- Subroutines ---

Rem     ----------------------------------------------------------------------

:CopyPDB
Echo Product: %1
If Exist %BldPath%\%1\Bin\*.pdb Copy %BldPath%\%1\Bin\*.pdb "%LnsServerDataDir%\DEBUG\bin" > %NUL%

Echo.
Goto :EOF

Rem     ----------------------------------------------------------------------


:done

popd
@echo FINISHED!

