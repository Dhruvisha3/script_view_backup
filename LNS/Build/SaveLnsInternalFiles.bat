@Echo Off

Rem     $Header$
Rem
Rem     (c) Copyright 2004-2012, Echelon Corporation.  All rights reserved.
Rem
Rem     SaveLnsInternalFiles.bat - Save internal files from OpenLNS builds that 
Rem		are needed for future debugging purposes.

Set Root="\\virgil\Shared SW\LNS Installations\Internal\Development"
If Not Exist %Root% MkDir %Root%

Set Dir=%Root%\%VerMajor%.%VerMinor%.%VerBuild%

Rem     wipe out previous folder and contents
rem If Exist %Dir% RmDir /S /Q %Dir%
rem If Exist %Root%\Redist RmDir /S /Q %Root%\Redist
rem If Exist %Dir% Goto :DirBusy
MkDir %Dir%
MkDir %Dir%\PDB
MkDir %Dir%\Map
MkDir %Dir%\Extras
MkDir %Root%\Redist
MkDir %Root%\Redist\Lib
MkDir %Root%\Redist\Include

Echo Copying internal use OpenLNS files to %Dir%...
Echo.
For %%d In (NSS DASVNI OBJ LCA) Do Call :CopyProd %%d
Call :Extras
Goto :Exit

Rem     ----------------------------------------------------------------------

:CopyProd
Echo Product: %1
If Exist %BldDir%\%1\Bin\*.pdb (
    xCopy /D /Y %BldDir%\%1\Bin\*.pdb %Dir%\PDB > %NUL%

    Rem     also save symbols to our Symbol Store
    Call SaveSym "%BldDir%\%1\Bin\*.pdb" /v "%VerMajor%.%VerMinor%.%VerBuild%" /c "LNS:%1"
)
If Exist %BldDir%\%1\Bin\*.map xCopy /D /Y %BldDir%\%1\Bin\*.map %Dir%\Map > %NUL%

Echo.
Goto :EOF

Rem     ----------------------------------------------------------------------

:Extras

echo extra: nodesim
Copy %BldDir%\nss\bin\nodesim.exe        %Dir%\Extras
Copy %BldDir%\nss\bin\nodesim.pdb        %Dir%\Extras
Copy %BldDir%\nss\bin\nodesimsupport.dll %Dir%\Extras
Copy %BldDir%\nss\bin\nodesimsupport.pdb %Dir%\Extras

echo extra: nssreg
Copy %BldDir%\nss\bin\nssreg.exe %Dir%\Extras
Copy %BldDir%\nss\bin\nssreg.pdb %Dir%\Extras

echo extra: lnsireg
Copy %BldDir%\OBJ\bin\lnsireg.exe %Dir%\Extras
Copy %BldDir%\OBJ\bin\lnsireg.pdb %Dir%\Extras

echo extra: done
Goto :EOF

Rem     ----------------------------------------------------------------------

:DirBusy
Echo %0: failed to delete folder - perhaps it's in use
Goto :Exit

:Exit
Rem     That's all Folks!
EndLocal
