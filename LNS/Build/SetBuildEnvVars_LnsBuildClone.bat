Rem     $Header$
Rem
Rem     (c) Copyright 2004-2012, Echelon Corporation.  All rights reserved.

Rem SetBuildEnvVars_%PCName%.bat - This file sets all of the variables
Rem	that are variable from one build PC to another.

Rem     Set names of various P4 branches and workspaces used by the build
Set P4LNSTOOLS=%P4LNSTOOLS%_LnsBuild
Set P4LNSBUILD=%P4LNSBUILD%_LnsBuild
Set P4LNSEXPORTBRANCH=Export_LNS_%P4CODELINE%
Set P4LNSEXPORTWORKSPACE=Export_LNS_%P4CODELINE%_LnsBuild_W

Set BldDrive=C:
Set ProjPath=\GitTools
set git_lnsdev=%BldDrive%%ProjPath%\lns\
set git_export=%BldDrive%%ProjPath%\export\
set git_component=%BldDrive%%ProjPath%\components\
set git_tools=%BldDrive%%ProjPath%\tools\

set git_lnsdev_branch=Dev
set git_export_branch=Dev
set git_component_branch=Dev

Rem  Set locations 

Set IsToolsDrive=C:
Set BldToolsDrive=C:
Set IsBldDrive=C:
Set CopyRootDrive=%IsBldDrive%
Set BldDir=%ProjPath%\lns\
Set BldPath=%BldDrive%%BldDir%
Set BinDir=%git_tools%Bin
Set PerlDir=C:\Perl64
Set IsToolsPath=%IsToolsDrive%\Program Files (x86)\InstallShield\2013 SP1 SAB\System
Set BldToolsPath=%BldPath%\Build
Set IsMergeModulePath=%BldPath%\import\MergeModules
Set IsProjPath=%BldDrive%%BldDir%\Install
Set ThirdPartyDir=%git_component%Others
Set LNISelfInstDir=%BldToolsDrive%\LNI\SelfInstallers
Set DotNETPath=C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319
Set COMMONINCLUDE=..\Import

Rem     Build directories
Set DevEnvDir=C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC
Set DevEnvToolsDir=C:\Program files (x86)\Microsoft Visual Studio 12.0\Common7\Tools
Set DevEnvBinDir=C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE;%DevEnvDir%\bin;%DevEnvToolsDir%
Set PlatformSDKDir=C:\Program Files (x86)\Microsoft SDKs\Windows\v8.1A

Set STLportDir=%ThirdPartyDir%\STLport\V5.2
Set STLportIncDir=%STLportDir%\STLport
Set STLportLibDir=%STLportDir%\Lib\vc12
Set STLportBinDir=%STLportDir%\Bin\vc12

Set BoostDir=%ThirdPartyDir%\Boost\V1.39
Set BoostIncDir=%BoostDir%
Rem Set BoostLibDir=%BoostDir%\{appropriate LIB path here}
Rem Set BoostBinDir=%BoostDir%\{appropriate BIN path here}

Rem     Redistributable directories
Set FastObjectsDir=C:\Program Files (x86)\Versant\FastObjects_t7_12.0

Rem	Build PATHs - Make this independent of the Build PC environment variables, this must be tightly controlled
Set ToolsPaths=%git_tools%Bin
Set WinPaths=C:\Windows\System32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0
Set PerforcePath=C:\Program Files\Perforce
set WinKitPath=C:\Program Files (x86)\Windows Kits
Set WinSdkPaths=%WinKitPath%;%WinKitPath%\8.1\Include\um;%WinKitPath%\10\Debuggers\x64;%WinKitPath%\8.1\Windows Performance Toolkit;%PlatformSDKDir%
Set LonWorksPath=C:\Program Files (x86)\LonWorks\bin
Set EfcPath=%git_export%EFC\Bin\vc12
Set BuildPath=%ToolsPaths%;%WinPaths%;%PerforcePath%;%WinSdkPaths%;%LonWorksPath%;%EfcPath%;%STLportBinDir%
Set IsBuildPath=%BuildPath%;%IsToolsPath%

