@Echo Off

Rem     $Header$
Rem
Rem     (c) Copyright 2004-2012, Echelon Corporation.  All rights reserved.

If "%1x" == "x" Goto syntax_check
Set /A DebugBuildNo=%1
Set /A ReleaseBuildNo=%DebugBuildNo% + 1

Echo Build start time:
Date /T
Time /T
Echo.

Rem NOTE:
Rem     Normally, ERRORLEVEL is set to the exit code
Rem     of the last command in a command pipeline, which
Rem     is not what we want below.  To work around this,
Rem     'SaveErr' saves the ERRORLEVEL of the specified
Rem     command, and 'RestErr' restores this last saved
Rem     ERRORLEVEL.

Echo ++++++++++++++++++++++++
Echo +++ OpenLNS Build (Only)
Echo ++++++++++++++++++++++++
Echo.

set BldPath=C:\GitTools\lns
Echo Build Path is : %BldPath%
pushD %BldPath%
    Call SaveErr Build\BuildLnsDebugAndRelease %1 2>&1 | Tee LnsBuild%DebugBuildNo%And%ReleaseBuildNo%.log & RestErr
PopD
If ErrorLevel 1 Goto done_error
Echo.


:successExit
    Echo ---------------------
    Echo OpenLNS Build Success
    Echo ---------------------
    Goto done

:syntax_check
    Echo **************************
    Echo BuildLnsOnly syntax:
    Echo	BuildLnsOnly <BuildNum>
    Echo **************************
    Goto done

:done_error
    Echo **********************
    Echo Error building OpenLNS
    Echo **********************
    Echo.

:done
    Echo Build End time:
    Date /T
    Time /t
