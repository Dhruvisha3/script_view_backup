@Echo Off

Rem     $Header$
Rem
Rem     Usage: SetAssemblyVer {filepath} {build}
Rem
Rem     � Copyright 2010-2011, Echelon Corporation.  All rights reserved.
Rem
Rem     This batch file replaces the build version numbers in the specified
Rem     AssemblyInfo.cs-style file.
Rem     
Rem     NOTE: Only the Build number will be modified here.  You must first
Rem           make sure that the Major and Minor version numbers are already
Rem           set correctly.

Rem     extract Assembly-style file path and build number from command line
If "%~2" == "" Echo Usage: %0 {filepath} {build} >&2 & Exit /B 1
Set AssemblyPath=%~1
Set BuildNum=%~2

Rem     ensure specified file exists
If Not Exist "%AssemblyPath%" Echo %AssemblyPath%: file not found! >&2 & Exit /B 2

Rem     update the version numbers of the the specified AssemblyInfo-style file
Perl -pi.bak ^
    -e "/Assembly(File)?Version/ and s/\.\d+\.0\"/.%BuildNum%.0\"/" ^
    "%AssemblyPath%"

Rem     That's all folks!
Exit /B 0
