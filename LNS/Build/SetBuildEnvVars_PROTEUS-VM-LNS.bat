Rem $Header$
Rem
Rem SetBuildEnvVars_%PCName%.bat - This file sets all of the variables
Rem	that are variable from one build PC to another.

Rem  Set locations 
Set BldDrive=E:
Set IsToolsDrive=C:
Set BldToolsDrive=C:
Set IsBldDrive=C:
Set CopyRootDrive=%IsBldDrive%
Set BldDir=\LNS\dev
Set BldPath=%BldDrive%%BldDir%
Set BinDir=C:\Bin
Set PerlDir=C:\Perl
Set IsToolsPath=%IsToolsDrive%\Program Files\MacroVision\IS 12 StandaloneBuild
Set BldToolsPath=%BldPath%\Build
Set IsProjDir=%BldDir%\InstallShield
Set IsProjPath=%IsBldDrive%%IsProjDir%
Set IsMergeModulePath=%ISProjPath%\MergeModules
Set IsSourceProjPath=%BldDrive%\%BldDir%\Install
Set CopyRootDir=%IsBldDrive%%BldDir%\InstallShield\InstallInput
Set ThirdPartyDir=%BldToolsDrive%\Components\Others
Set LNISelfInstDir=%BldToolsDrive%\LNI\SelfInstallers
Set COMMONINCLUDE=..\Import

Rem     Build directories
Set MsDevDir=C:\Program Files\Microsoft Visual Studio\VC98
Set DevEnvDir=C:\Program Files\Microsoft Visual Studio 8\VC
Set STLportDir=%ThirdPartyDir%\STLport\V5.1
Set STLportIncDir=%STLportDir%\STLport
Set STLportLibDir=%STLportDir%\Lib\vc8
Set VC6STLportDir=%MSDevDir%\STLport
Set DevEnvToolsDir=C:\Program files\Microsoft Visual Studio 8\Common7\Tools\bin
Set DevEnvBinDir=C:\Program Files\Microsoft Visual Studio 8\Common7\IDE;%DevEnvDir%\bin;%DevEnvToolsDir%
Set PlatformSDKDir=C:\Program Files\Microsoft SDK
Set FastObjectsDir=C:\Program Files\Versant\FastObjects_t7_10.0
Set FastObjectsBaseDir=C:\Program Files\Versant\FastObjects_Databases\base
