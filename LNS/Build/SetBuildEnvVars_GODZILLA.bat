rem
rem  SetBuildEnvVars_%PCName%.bat - This file sets all of the variables
rem	that are variable from one build PC to another.
rem

rem  Set locations 
Set BldDrive=e:
Set IsToolsDrive=C:
Set BldToolsDrive=C:
Set IsBldDrive=C:
Set CopyRootDrive=%IsBldDrive%
Set BldDir=\LNS\dev
Set BldPath=%BldDrive%%BldDir%
Set BinDir=C:\Bin
Set PerlDir=C:\Perl
Set IsToolsPath=%IsToolsDrive%\Program Files\MacroVision\IS 12 StandaloneBuild
Set BldToolsPath=%BldPath%\Build
Set IsProjDir=%BldDir%\InstallShield
Set IsProjPath=%IsBldDrive%%IsProjDir%
Set IsMergeModulePath=%ISProjPath%\MergeModules
Set IsSourceProjPath=%BldDrive%\%BldDir%\Install
Set CopyRootDir=%IsBldDrive%%BldDir%\InstallShield\InstallInput
Set ThirdPartyDir=%BldToolsDrive%\Components\Others
Set LNISelfInstDir=%BldToolsDrive%\LNI\SelfInstallers
Set COMMONINCLUDE=..\Import

rem     Build directories
Set MsDevDir=C:\Program Files\Microsoft Visual Studio\VC98
Set DevEnvDir=C:\Program Files\Microsoft Visual Studio 8\VC
Set STLportDir=%DevEnvDir%\STLport
Set VC6STLportDir=%MSDevDir%\STLport
Set DevEnvToolsDir=c:\Program files\Microsoft Visual Studio 8\Common7\Tools\bin
Set DevEnvBinDir=C:\Program Files\Microsoft Visual Studio 8\Common7\IDE;%DevEnvDir%\bin;%DevEnvToolsDir%
Set PlatformSDKDir=C:\Program Files\Microsoft SDK
Set FastObjectsDir=C:\Program Files\Versant\FastObjects_t7_10.0
Set FastObjectsBaseDir=C:\Program Files\Versant\FastObjects_Databases\base

