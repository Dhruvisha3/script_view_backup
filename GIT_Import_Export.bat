@echo off
setlocal enabledelayedexpansion

set viewfilepath=%1
set description=%2
set /a commit=0
set curcd=%~dp0

for /f "usebackq tokens=1,2* delims= " %%I in ("%viewfilepath%") do (
	if %%I == project: (				
		call :setpath %%J %%K
		set /a commit=1
	) else (
		set src=%%I
		set dest=%%J
		set "src=!src:?= !"
		if Not [%%J]==[] set "dest=!dest:?= !"
		call :filecopy "!src!" "!dest!"
	)
	
	if ErrorLevel 1 goto :done_error
)

call :setpath
cd "%curcd%"
EXIT /B %ERRORLEVEL%

:filecopy	
	if not Exist "!%source%!%~1" EXIT /B 0
	echo f|xcopy /f /Y /q "!%source%!%~1" "!%target%!%~2"
		if ErrorLevel 1 Goto copy_error		
	
	if ["%~nx2"]==[] (
		set source_filename=%~nx1		
		git add "!%target%!%~2!source_filename!"
	) else (
		git add "!%target%!%~2"
	)
	if ErrorLevel 1 Goto add_error
	EXIT /B 0

:setpath
	if %commit%==1 (		
		git commit -m %description%
		echo.
		echo ### Push files to remote ###
		echo ###----------------------###
		git push origin !%target%_branch!
			if ErrorLevel 1 Goto push_error	
		echo ###----------------------###
rem		echo.
rem		echo ### Revert stash changes if stored ###
rem		echo ###--------------------------------###		
rem		git stash apply
rem		echo ###--------------------------------###
rem		echo.	
		echo End of copy files
		echo *******************************************************
	)
	
	if [%1]==[] EXIT /B 0
		
	set source=%1
	set target=%2
	
	echo. 
	echo *******************************************************
	echo  Copy files from "%source%: !%source%_branch!" to "%target%: !%target%_branch!"
	echo *******************************************************
	echo.
	
	cd "!%source%!" 
	git pull
		if ErrorLevel 1 Goto pull_error
	git checkout !%source%_branch!
		if ErrorLevel 1 Goto checkout_error
	git pull origin !%source%_branch!
		if ErrorLevel 1 Goto checkout_error
	
	cd "!%target%!" 
	git pull	
		if ErrorLevel 1 Goto pull_error
	echo.
rem	echo ### Stash the changes if found ###
rem	echo ###----------------------------###
rem	git stash
rem	echo ###----------------------------###
rem	echo.
	git checkout !%target%_branch!
		if ErrorLevel 1 Goto checkout_error
	git pull origin !%target%_branch!
	EXIT /B 0
	
	
:successExit
ECHO Imported SucessFully
goto done
		
:pull_error
echo No such Git repositoty 
echo first clone the repositoty in your local if exist 
Exit /B 1

:checkout_error
echo No such branch on provided repositoty 
Exit /B 1

:push_error
echo echo There is an error in push data to GIT on branch !%target%_branch!
Exit /B 1

:copy_error
echo While Copy......No such file or directoty 
Exit /B 1

:add_error
echo Error generated while adding the file......Provide correct file name
Exit /B 1

:done_error
Echo Error Importing 
Exit /B 1

:done
Exit /B 0